void instantQuit()
{
	exit(0);
}

void newInstance()
{
	system("dvmedia &");
}


void testcb()
{
	printf("\ntest\n");
	fflush(stdout);
}


void toggleFullscreen()
{
	createFullscreenWindow(videoEntry0, (XtPointer)topLevel, (XtPointer)topLevel);
}


void toggleMute()
{
	if(isMuted == 1) /* if control area is visible.. */
	{
		isMuted = 0; /* toggle global flag */
		
		mpv_set_option_string(mpv, "mute", "no");
		
		XtVaSetValues(statusLabel2, XmNlabelPixmap, 0, NULL);
		
		XtVaSetValues(statusLabel2,
			XmNlabelPixmap, mute0,
		NULL);
		
		XtVaSetValues(audioEntry0,
			XtVaTypedArg, XmNlabelString, XmRString, "Mute", 4,
			XmNmnemonic, 'M',
		NULL);
	}
	
	else if(isMuted == 0) /* if not shown.. */
	{
		isMuted = 1;
		
		mpv_set_option_string(mpv, "mute", "yes");
		
		XtVaSetValues(statusLabel2, XmNlabelPixmap, 0, NULL);
		
		XtVaSetValues(statusLabel2,
			XmNlabelPixmap, mute1,
		NULL);
		
		XtVaSetValues(audioEntry0,
			XtVaTypedArg, XmNlabelString, XmRString, "Unmute", 4,
			XmNmnemonic, 'm',
		NULL);
	}
}


void aboutButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	
	char notifierStrings[1024];
	snprintf(notifierStrings, sizeof(notifierStrings), "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
	
		"DeskView Media Player \n",
		"Version: ", versionString, "\n",
		"Date: ", dateString, "\n\n",
		
		"Core Library: ", libmpvVersion, "   \n",
		"Hardware Decode: ", hasHardwareAccel, "\n",
		"Video Device: ", videoOutputDev, "\n",
	NULL);
	
	char cmdBuffer[1024];
	snprintf(cmdBuffer, sizeof(cmdBuffer), "%s", notifierStrings);
	
	createInfoDialog(callingWidget, topLevel, "About", notifierStrings, 1, 1, 0);
}

void licenseButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	
	char notifierStrings[1024];
	snprintf(notifierStrings, sizeof(notifierStrings), "%s%s%s%s",
	
		"The GUI frontend is distributed free of charge \n",
		"under the BSD Zero Clause license. \n\n",
		
		"The mpv core is GPLv2+ licensed and the client \nheader is ISC.\n",
		
	NULL);
	
	char cmdBuffer[1024];
	snprintf(cmdBuffer, sizeof(cmdBuffer), "%s", notifierStrings);
	
	createInfoDialog(callingWidget, topLevel, "Usage", notifierStrings, 0, 1, 0);
}

void usageButtonCallback(Widget callingWidget, XtPointer clientData, XtPointer callData)
{
	Widget topLevel = (Widget)clientData;
	
	char notifierStrings[1024];
	snprintf(notifierStrings, sizeof(notifierStrings), "%s%s%s%s%s",
	
		"Space key or right clicking in the video area \n",
		"should toggle playback. \n",
		
		"\nThe underlined menu mnemonics are typically \n",
		"bound to the Alt key. Alt + F should activate \n",
		"the File dropdown pane. \n",
		
	NULL);
	
	char cmdBuffer[1024];
	snprintf(cmdBuffer, sizeof(cmdBuffer), "%s", notifierStrings);
	
	createInfoDialog(callingWidget, topLevel, "Usage", notifierStrings, 0, 1, 0);
}


void toggleLowerControlArea(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;
	
	if(controlAreaVisible == 1) /* if control area is visible.. */
	{
		controlAreaVisible = 0; /* toggle global flag */
		
		XtVaSetValues(controlForm,
			XmNmappedWhenManaged, 0,
		NULL);
		
		XtVaSetValues(videoForm,
			XmNbottomAttachment, XmATTACH_FORM,
		NULL);
		
		XtVaSetValues(winEntry2,
			XtVaTypedArg, XmNlabelString, XmRString, "Show Lower Control Area", 4,
		NULL);
	}
	
	else if(controlAreaVisible == 0) /* if not shown.. */
	{
		controlAreaVisible = 1;
		
		XtVaSetValues(controlForm,
			XmNmappedWhenManaged, 1,
		NULL);
		
		XtVaSetValues(videoForm,
			XmNbottomAttachment, XmATTACH_WIDGET,
			XmNbottomWidget, controlForm,
		NULL);
		
		XtVaSetValues(winEntry2,
			XtVaTypedArg, XmNlabelString, XmRString, "Hide Lower Control Area", 4,
		NULL);
	}
}

/* trim off everything before the last directory */
char *trimPathSeparators(const char *file_path)
{
	const char *last_slash = strrchr(file_path, '/');
	if(last_slash != NULL)
	{
		size_t len = strlen(last_slash);
        
		char *trimmed_path = (char *)malloc(len + 1);
		if(trimmed_path != NULL)
		{
			strncpy(trimmed_path, last_slash + 1, len);
			trimmed_path[len] = '\0';
			return trimmed_path;
		}
	
		else { return NULL; }
	}
	
	else
	{
		char *original_path = (char *)malloc(strlen(file_path) + 1);
		if(original_path != NULL)
		{
			strcpy(original_path, file_path);
			return original_path;
		}
		
		else { return NULL; }
	}
}


void resetCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	if(!fileIsLoaded)
	{
		createQuickErrorDialog("NONE", "No file loaded. Unable to reload media. ");
		return;
	}
	
	endOfFile = 0;
	isPlaying = 0;
	
	mpv_handle *mpv = (mpv_handle *)client_data;
	
	mpv_command_string(mpv, "loadfile \"\""); /* unloads current file if any */
	
	mpv_command_string(mpv, "set pause yes");
	
	XtVaSetValues(statusLabel,
		XtVaTypedArg, XmNlabelString, XmRString, "Paused", 4,
	NULL);
	
	XtVaSetValues(togglePlaybackButton,
		XtVaTypedArg, XmNlabelString, XmRString, "Begin Playback", 4,
	NULL);
	
	XtVaSetValues(pauseButton,
		XmNlabelPixmap, resume_pixmap,
	NULL);
	
	
	char tmpBuffer[4096];
	snprintf(tmpBuffer, sizeof(tmpBuffer), "loadfile \"%s\"", fileInputString);
	mpv_command_string(mpv, tmpBuffer);
	
	char *trimmed_path = trimPathSeparators(fileInputString);
	if (trimmed_path != NULL)
	{
		char titleBuffer[1024];
		snprintf(titleBuffer, sizeof(titleBuffer), "Media Player - %s", trimmed_path),
		
		XtVaSetValues(topLevel,
			XmNtitle, titleBuffer,
		NULL);
		
		free(trimmed_path);
	}
	
	wasReset = 1;
}


void toggleInfiniteLoop(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;
	
	if(infLoop == 0)
	{
		infLoop = 1;
		mpv_set_option_string(mpv, "loop-file", "inf");
		
		XtVaSetValues(infiniteLoop,
			XtVaTypedArg, XmNlabelString, XmRString, "Stop Looping", 4,
		NULL);
		
		XtVaSetValues(statusLabel1, XmNlabelPixmap, 0, NULL);
		
		XtVaSetValues(statusLabel1,
			XmNlabelPixmap, inf_loop,
			XmNmarginLeft, 3,
		NULL);
		
		XmUpdateDisplay(statusLabel1);
	}
	
	else if(infLoop == 1)
	{
		infLoop = 0;
		mpv_set_option_string(mpv, "loop-file", "no");
		
		XtVaSetValues(infiniteLoop,
			XtVaTypedArg, XmNlabelString, XmRString, "Loop Infinitely", 4,
		NULL);
		
		XtVaSetValues(statusLabel1, XmNlabelPixmap, 0, NULL);
		
		XtVaSetValues(statusLabel1,
			XmNlabelPixmap, play_once,
			XmNmarginLeft, 0,
		NULL);
		
		XmUpdateDisplay(statusLabel1);
	}
	
}


void seekLeft(Widget widget, XtPointer client_data, XtPointer call_data)
{
	if(!fileIsLoaded)
	{
		createQuickErrorDialog("NONE", "No file loaded. Unable to seek left. ");
		return;
	}
	
	mpv_handle *mpv = (mpv_handle *)client_data;
	
	char cmdBuffer[64];
	snprintf(cmdBuffer, sizeof(cmdBuffer), "seek -%f exact", seekSeconds);
	
	mpv_command_string(mpv, cmdBuffer);
}

void seekRight(Widget widget, XtPointer client_data, XtPointer call_data)
{
	if(!fileIsLoaded)
	{
		createQuickErrorDialog("NONE", "No file loaded. Unable to seek right. ");
		return;
	}
	
	mpv_handle *mpv = (mpv_handle *)client_data;
	
	char cmdBuffer[64];
	snprintf(cmdBuffer, sizeof(cmdBuffer), "seek +%f exact", seekSeconds);
	
	mpv_command_string(mpv, cmdBuffer);
}


void togglePlayback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;
	
	endOfFile = 0;
	wasReset = 0;
	
	if(!fileIsLoaded)
	{
		createQuickErrorDialog("NONE", "No file loaded. Unable to resume playback. ");
		return;
	}
	
	else if(isPlaying == 1) /* if already playing.. */
	{
		/* pause playback */
		mpv_command_string(mpv, "set pause yes");
		isPlaying = 0; /* toggle global flag */
		
		XtVaSetValues(pauseButton,
			XmNlabelPixmap, resume_pixmap,
		NULL);
		
		XtVaSetValues(statusLabel,
			XtVaTypedArg, XmNlabelString, XmRString, "Paused", 4,
		NULL);
		
		XtVaSetValues(togglePlaybackButton,
			XtVaTypedArg, XmNlabelString, XmRString, "Resume Playback", 4,
		NULL);
	}
	
	else if(isPlaying == 0) /* if not playing.. */
	{
		/* resume playback */
		mpv_command_string(mpv, "set pause no");
		isPlaying = 1;
		
		XtVaSetValues(pauseButton,
			XmNlabelPixmap, pause_pixmap,
		NULL);
		
		XtVaSetValues(statusLabel,
			XtVaTypedArg, XmNlabelString, XmRString, "Playing", 4,
		NULL);
		
		XtVaSetValues(togglePlaybackButton,
			XtVaTypedArg, XmNlabelString, XmRString, "Stop Playback", 4,
		NULL);
	}
}


void scaleCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	AppData *app_data = (AppData *)client_data;
	mpv_handle *mpv = app_data->mpv;
	Widget scale = app_data->scale;
	
	/* get scale value */
	int scale_value;
	XtVaGetValues(scale, XmNvalue, &scale_value, NULL);
	if(scale_value > 0)
	{
		/* convert the scale value back to seconds */
		double position_seconds = (double)scale_value / 1000.0; /* scale range is 0-1000 */
	
		/* seek to the specified position in seconds */
		char command[2048];
		snprintf(command, sizeof(command), "seek %f absolute", position_seconds);
		mpv_command_string(mpv, command);
		
	}
}

void format_time(char *buffer, int buffer_size, double time)
{
	int hours = (int)(time / 3600);
	int minutes = (int)((time / 60)) % 60;
	int seconds = (int)(time) % 60;
	snprintf(buffer, buffer_size, "%02d:%02d:%02d", hours, minutes, seconds);
}

/* update the scale with the current playback time */
void update_scale(XtPointer client_data, XtIntervalId *id)
{
	AppData *app_data = (AppData *)client_data;
	mpv_handle *mpv = app_data->mpv;
	Widget scale = app_data->scale;
	Widget label = app_data->label;
	double current_time = 0;
	double duration = 0;
	char current_time_str[32];
	char duration_str[32];
	char label_text[64];
	
	
	/* get current playback time */
	mpv_node result;
	if(mpv_get_property(mpv, "filename", MPV_FORMAT_NODE, &result) >= 0) 
	{
        	mpv_free_node_contents(&result);
		
		/* get total duration */
		if(mpv_get_property(mpv, "duration", MPV_FORMAT_NODE, &result) >= 0)
		{
			if(result.format == MPV_FORMAT_DOUBLE)
			{
				double duration = result.u.double_;
				/* multiplying by 1000 makes scale movements smoother */
				XtVaSetValues(scale, XmNmaximum, (int)(duration * 1000), NULL);
				
				XtVaSetValues(scale, XmNscaleMultiple, (int)(duration * 1000 / 10), NULL);
				
				format_time(duration_str, sizeof(duration_str), duration);
			}
			
			mpv_free_node_contents(&result);
		}
		
		if(mpv_get_property(mpv, "time-pos", MPV_FORMAT_NODE, &result) >= 0)
		{
			if(result.format == MPV_FORMAT_DOUBLE)
			{
				double current_time = result.u.double_;
				XtVaSetValues(scale, XmNvalue, (int)(current_time * 1000), NULL);
				
				format_time(current_time_str, sizeof(current_time_str), current_time);
			}
			
			mpv_free_node_contents(&result);
		}
		
		/* update label */
		snprintf(label_text, sizeof(label_text), "%s / %s", current_time_str, duration_str);
		XtVaSetValues(label, XmNlabelString, XmStringCreateLocalized(label_text), NULL);
	}
	
	
	mpv_event *event = mpv_wait_event(mpv, 0);
	if (event->event_id == MPV_EVENT_END_FILE && endOfFile == 0)
	{
		if(!wasReset)
		{
			resetCallback(label, (XtPointer)mpv, (XtPointer)mpv);
			wasReset = 1;
		}
		
		endOfFile = 1;
	}
	
	/* add next timer callback */
	XtAppAddTimeOut(XtWidgetToApplicationContext(scale), scaleUpdate_msec, update_scale, client_data);
}

void setup_mpv(mpv_handle *mpv, Window window)
{
	int64_t wid = (int64_t)window;
	
	#ifdef _NO_HARDWARE_ACCEL
	
	mpv_set_option_string(mpv, "vo", "x11");
	mpv_set_option_string(mpv, "hwdec", "no");
	
	snprintf(hasHardwareAccel, sizeof(hasHardwareAccel), "No ");
	snprintf(videoOutputDev, sizeof(videoOutputDev), "X11 ");
	
	#else
	
	mpv_set_option_string(mpv, "vo", "gpu");
	mpv_set_option_string(mpv, "hwdec", "auto-safe");
	
	snprintf(hasHardwareAccel, sizeof(hasHardwareAccel), "Yes ");
	snprintf(videoOutputDev, sizeof(videoOutputDev), "GPU ");
	
	#endif
	
	mpv_set_option(mpv, "wid", MPV_FORMAT_INT64, &wid);
	mpv_set_option_string(mpv, "ytdl", "no");
	mpv_set_option_string(mpv, "osc", "no");
	mpv_set_option_string(mpv, "osd-bar", "no");
	mpv_set_option_string(mpv, "input", "no");
	mpv_set_option_string(mpv, "cache", "auto");
	mpv_set_option_string(mpv, "cache-on-disk", "no");
	mpv_set_option_string(mpv, "async", "yes");
	
	mpv_initialize(mpv);
	
/* grab version string */
	const char *mpv_version = mpv_get_property_string(mpv, "mpv-version");
	snprintf(libmpvVersion, sizeof(libmpvVersion), "lib%s", mpv_version);
	
	
	
	
/* 
	=======================================
	||                                   ||
	||  Extra options to look at later   ||
	||                                   ||
	=======================================
*/
	
	/*
	mpv_set_option_string(mpv, "panscan", "1.0");
	*/
	
	/*
	mpv_set_option_string(mpv, "audio-channels", "stereo");
	*/
	
	/*
	mpv_set_option_string(mpv, "video-unscaled", "downscale-big");
	mpv_set_option_string(mpv, "video-unscaled", "no");
	
	mpv_set_option_string(mpv, "target-prim", "v-gamut");
	*/
	
	/*
	mpv_command_string(mpv, "keep-aspect no");
	mpv_set_property_string(mpv, "video-aspect", "4:3");
	*/
	
	if(openedWithFile) /* if program was opened on a file or url... */
	{
		char tmpBuffer[4096];
		snprintf(tmpBuffer, sizeof(tmpBuffer), "loadfile \"%s\"", fileInputString);
		mpv_command_string(mpv, tmpBuffer);
		
		char *trimmed_path = trimPathSeparators(fileInputString);
		if(trimmed_path != NULL)
		{
			char titleBuffer[1024];
			snprintf(titleBuffer, sizeof(titleBuffer), "Media Player - %s", trimmed_path),
		
			XtVaSetValues(topLevel,
				XmNtitle, titleBuffer,
			NULL);
			
			free(trimmed_path);
		}
		
		XtVaSetValues(togglePlaybackButton,
			XtVaTypedArg, XmNlabelString, XmRString, "Stop Playback", 4,
		NULL);
		
		fileIsLoaded = 1;
	}
	
	if(!openedWithFile)
	{
		/* pause playback */
		mpv_command_string(mpv, "set pause yes");
		isPlaying = 0; /* toggle global flag */
		fileIsLoaded = 0;
		
		XtVaSetValues(pauseButton,
			XmNlabelPixmap, resume_pixmap,
		NULL);
		
		XtVaSetValues(statusLabel,
			XtVaTypedArg, XmNlabelString, XmRString, "No File", 4,
		NULL);
		
	/* disable unusable widgets when no file is loaded */
		int widgetSensitivity = 0;
		XtVaSetValues(fileDetails, XmNsensitive, widgetSensitivity, NULL);
		
		XtVaSetValues(togglePlaybackButton, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(infiniteLoop, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(playbackSpeed, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(seekValue, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(seekBackward, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(seekForward, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(resetReload, XmNsensitive, widgetSensitivity, NULL);
		
		XtVaSetValues(audioEntry0, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(audioEntry1, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(audioEntry2, XmNsensitive, widgetSensitivity, NULL);
		XtVaSetValues(audioEntry3, XmNsensitive, widgetSensitivity, NULL);
	}
}


/* on right click */
void rightClickCallback(Widget widget, XtPointer clientData, XEvent* event, Boolean* NONE)
{
	AppData *app_data = (AppData *)clientData;
	mpv_handle *mpv = app_data->mpv;
	/* Widget popup = app_data->popup; */
	
	/* if there was a button press event and it was a right click... */
	if(event->type == ButtonPress && event->xbutton.button == Button3)
	{
		togglePlayback(NULL, (XtPointer)mpv, (XtPointer)mpv);
		
		/* if shift or control modifier key was used * /
		if(event->xbutton.state & ShiftMask || event->xbutton.state & ControlMask) 
		{
			
			printf("\n Intercepted modified right click! \n");
			fflush(stdout);
			
			/*
			Widget popupMenu = (Widget)popup;
			XmMenuPosition(popupMenu, (XButtonPressedEvent*)event);
			XtManageChild(popupMenu);
			* /
		}
		
		else /* for unmodified right clicks * /
		{
			togglePlayback(NULL, (XtPointer)mpv, (XtPointer)mpv);
		}
		*/
	}
}


int keepAbove = 0;
void *keepWindowAbove()
{
	while(keepAbove) /* while keepAbove is 1 (true)... */
	{
		/* get Xt display and window */
		Display *display = XtDisplay(topLevel);
		Window window = XtWindow(topLevel);
	
		/* bring window to top of stack */
		XRaiseWindow(display, window);
		/* repaint display */
		XFlush(display);
		
		/* take nap */
		usleep(800000);
	}
}

void toggleAlwaysOnTop()
{
	pthread_t keepAboveThread;
	
	if(!keepAbove) /* if keepAbove other windows is false... */
	{
		/* toggle thread flag */
		keepAbove = 1;
		/* spawn another POSIX thread */
		pthread_create(&keepAboveThread, NULL, keepWindowAbove, NULL);
		
		/* set label text on menu entry */
		XtVaSetValues(winEntry0, XtVaTypedArg, XmNlabelString, XmRString, "Stack Window Normally", 4, NULL);
	}
	
	else if(keepAbove) /* else if true... */
	{
		keepAbove = 0;
		pthread_join(keepAboveThread, NULL); /* removes thread */
		
		XtVaSetValues(winEntry0, XtVaTypedArg, XmNlabelString, XmRString, "Keep Top of Stack", 4, NULL);
	}
}




int *getFrameExtents(Widget widget)
{
	/* this array is for the border sizes in pixels */
	static int extents[4] = {0, 0, 0, 0};

	/* grab display and window associated with input widget */
	Display *display = XtDisplay(widget);
	Window window = XtWindow(widget);

	/* sets the XAtom we're looking for */
	Atom property = XInternAtom(display, "_NET_FRAME_EXTENTS", True);

	/* return nothing and exit function if prop not found */
	if(property == None)
	{
		printf("\n Could not find _NET_FRAME_EXTENTS property. \n");
		fflush(stdout);
		
		return NULL;
	}

	/* if property was found, fetch values */
	Atom actualType;
	int actualFormat;
	unsigned long itemCount, bytesAfter;
	unsigned char *propReturn = NULL;
	
	if(XGetWindowProperty(display, window, property, 0, 4, False, XA_CARDINAL,
	&actualType, &actualFormat, &itemCount, &bytesAfter, &propReturn) == Success)
	{
		if(actualType == XA_CARDINAL && actualFormat == 32 && itemCount == 4)
		{
			long *extentsLong = (long *)propReturn;
			extents[0] = (int)extentsLong[0]; /* left */
			extents[1] = (int)extentsLong[1]; /* right */
			extents[2] = (int)extentsLong[2]; /* top - includes title bar */
			extents[3] = (int)extentsLong[3]; /* bottom */
		}
		
		/* clean up */
		XFree(propReturn);
	}

	/* send out results */
	return extents;
}


void centerWindow(Widget widget, XtPointer client_data, XEvent *event, Boolean *cont)
{
/* 
	This is needed since iconify/restore operations from
	the window manager will trigger a MapNotify event all
	over again. This will result in super annoyed users
	who expect the window to be where they left it.
*/
	if(centeredWindowFirstRun)
	{
		return; /* do nothing if window was already centered at startup */
	}
	
	
/* if top shell has been fully mapped... */
	if(event->type == MapNotify)
	{
		centeredWindowFirstRun = 1;
		
		/* get _NET_FRAME_EXTENTS for window border and title bar */
		int borderTop, borderLeft, borderRight, borderBottom;
		int *extents = getFrameExtents(widget);
		if(extents != NULL)
		{
			borderTop = extents[2];
			borderLeft = extents[0];
			borderRight = extents[1];
			borderBottom = extents[3];
		}
		
		/* grab display and window associated with input widget */
		Display *display = XtDisplay(widget);
		Window window = XtWindow(widget);
		
		/* grab screen count from xinerama */
		int screen_count;
		XineramaScreenInfo *screen_info = XineramaQueryScreens(display, &screen_count);
		
		/* if no screens... */
		if(!screen_info || screen_count == 0)
		{
			fprintf(stderr, "\n Could not retrieve Xinerama screen info. \n");
			fflush(stderr);
			
			XFree(screen_info);
			return;
		}
		
		/* grab root window from display (topLevel) */
		RootWindow(display, screen_info[0].screen_number);
		
		/* grab dimensions of input widget */
		Dimension winWidth, winHeight;
		XtVaGetValues(widget,
			XmNwidth, &winWidth,
			XmNheight, &winHeight,
		NULL);
		
		/* calculate new window position */
		int newX = (screen_info[0].width / 2) - winWidth / 2 - borderLeft;
		int newY = (screen_info[0].height / 2) - winHeight / 2 - (borderBottom * 2);
		
		/* move window to new position */
		XMoveWindow(display, window, newX, newY);
		
		/* clean up */
		XFree(screen_info);
	}
}










