/* experimental and not that useful but I had fun making it */


void togglePercBar()
{
	Pixel oldFG, oldBG;
	
	XtVaGetValues(menuBar,
		XmNforeground, &oldFG,
		XmNbackground, &oldBG,
	NULL);
	
	if(percBarVisible) /* if bar is enabled... */
	{
		percBarVisible = 0;
		
		XtVaSetValues(sliderJumpButtonContainer,
			XmNmappedWhenManaged, 0,
		NULL);
		
		XtVaSetValues(controlButtonContainer,
			XmNtopAttachment, XmATTACH_WIDGET,
			XmNtopWidget, sliderForm,
		NULL);
		
		XtVaSetValues(sliderForm,
			XmNbackground, oldBG,
		NULL);
		
		XtVaSetValues(winEntry3,
			XtVaTypedArg, XmNlabelString, XmRString, "Show Jump Bar", 4,
		NULL);
		
		/* fixup window min height */
		Dimension menuBar_h, controlForm_h;
		XtVaGetValues(menuBar, XmNheight, &menuBar_h, NULL);
		XtVaGetValues(controlForm, XmNheight, &controlForm_h, NULL);
	
		XtVaSetValues(topLevel,
			XmNminHeight, menuBar_h + controlForm_h,
		NULL);
	}
	
	else if(!percBarVisible) /* if disabled... */
	{
		percBarVisible = 1;
		
		XtVaSetValues(sliderJumpButtonContainer,
			XmNmappedWhenManaged, 1,
		NULL);
		
		XtVaSetValues(controlButtonContainer,
			XmNtopAttachment, XmATTACH_WIDGET,
			XmNtopWidget, sliderJumpButtonContainer,
		NULL);
		
		XtVaSetValues(sliderForm,
			XmNbackground, oldFG,
		NULL);
		
		XtVaSetValues(winEntry3,
			XtVaTypedArg, XmNlabelString, XmRString, "Hide Jump Bar", 4,
		NULL);
		
		Dimension menuBar_h, controlForm_h;
		XtVaGetValues(menuBar, XmNheight, &menuBar_h, NULL);
		XtVaGetValues(controlForm, XmNheight, &controlForm_h, NULL);
	
		XtVaSetValues(topLevel,
			XmNminHeight, menuBar_h + controlForm_h,
		NULL);
	}
	
}


void percBar0cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 5 absolute-percent");
}
void percBar1cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 15 absolute-percent");
}
void percBar2cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 25 absolute-percent");
}
void percBar3cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 35 absolute-percent");
}
void percBar4cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 45 absolute-percent");
}
void percBar5cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 55 absolute-percent");
}
void percBar6cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 65 absolute-percent");
}
void percBar7cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 75 absolute-percent");
}
void percBar8cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 85 absolute-percent");
}
void percBar9cb(Widget widget, XtPointer client_data, XtPointer call_data)
{
	mpv_handle *mpv = (mpv_handle *)client_data;	
	mpv_command_string(mpv, "seek 95 absolute-percent");
}


void createPercBar(Widget parent)
{
	int percBarHeight = 12;
	int percBarShadow = 2;
	
	percBar0 = XtVaCreateManagedWidget("percBar0", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 10,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar1 = XtVaCreateManagedWidget("percBar1", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar0,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 20,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar2 = XtVaCreateManagedWidget("percBar2", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar1,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 30,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar3 = XtVaCreateManagedWidget("percBar3", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar2,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 40,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar4 = XtVaCreateManagedWidget("percBar4", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar3,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 50,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar5 = XtVaCreateManagedWidget("percBar5", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar4,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 60,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar6 = XtVaCreateManagedWidget("percBar6", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar5,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 70,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar7 = XtVaCreateManagedWidget("percBar7", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar6,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 80,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar8 = XtVaCreateManagedWidget("percBar8", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar7,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 90,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
	
	percBar9 = XtVaCreateManagedWidget("percBar9", xmPushButtonWidgetClass, parent,
		XmNshadowThickness, percBarShadow,
		XmNtraversalOn, 0,
		XmNheight, percBarHeight,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, percBar8,
		XmNleftOffset, -2,
		XmNrightAttachment, XmATTACH_POSITION,
		XmNrightPosition, 100,
		
		XtVaTypedArg, XmNlabelString, XmRString, "", 4,
	NULL);
}
