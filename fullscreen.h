
void destroyFullscreenWindow(Widget widget, XtPointer client_data, XtPointer call_data)
{
	Widget fullscreenVideoShell = (Widget)client_data;
	XtDestroyWidget(fullscreenVideoShell);
}

void mouseEnteredFrame(Widget widget, XtPointer client_data, XEvent *event, Boolean *bool)
{
	Widget cancelButton = (Widget)client_data;
	
	XtVaSetValues(cancelButton,
		XtVaTypedArg, XmNforeground, XmRString, "gold", 4,
		XtVaTypedArg, XmNbackground, XmRString, "gray30", 4,
		XtVaTypedArg, XmNarmColor, XmRString, "darkred", 4,
		XmNheight, 60,
	NULL);
}

void mouseLeftFrame(Widget widget, XtPointer client_data, XEvent *event, Boolean *bool)
{
	Widget cancelButton = (Widget)client_data;
	
	XtVaSetValues(cancelButton,
		XtVaTypedArg, XmNbackground, XmRString, "black", 4,
		XmNheight, 1,
	NULL);
}

void createFullscreenWindow(Widget widget, XtPointer client_data, XtPointer call_data)
{
/* initialize a bare dialog shell */
	fullscreenVideoShell = XmCreateDialogShell(topLevel, "fullscreenVideoShell", NULL, 0);
	XtVaSetValues(fullscreenVideoShell,
		XmNoverrideRedirect, 1, /* do not interact with window manager */
		XmNmwmDecorations, 0, /* disable all window decorations and menus */
	NULL);
	
/* create manager form */
	Widget fullscreenForm = XtVaCreateManagedWidget("fullscreenForm", xmFormWidgetClass, fullscreenVideoShell,
		XmNshadowThickness, 0,
		XtVaTypedArg, XmNbackground, XmRString, "black", 4,
	NULL);
	
	
	Widget cancelButton = XtVaCreateManagedWidget("cancelButton", xmPushButtonWidgetClass, fullscreenForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		
		XmNheight, 1,
		XmNtraversalOn, 0,
		XmNshadowThickness, 0,
		
		XtVaTypedArg, XmNbackground, XmRString, "black", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Click Here to Exit Fullscreen Mode", 4,
	NULL);
	
	XtAddCallback(cancelButton, XmNactivateCallback, destroyFullscreenWindow, (XtPointer)fullscreenVideoShell);
	
	XtAddEventHandler(cancelButton, EnterWindowMask, False, mouseEnteredFrame, (XtPointer)cancelButton);
	XtAddEventHandler(cancelButton, LeaveWindowMask, False, mouseLeftFrame, (XtPointer)cancelButton);
	
	
	createQuickErrorDialog("Error", "Fullscreen support is not available yet. Mouse over the top of the overlay screen to show the exit button.");
	
	
	XtVaSetValues(fullscreenVideoShell,
		XmNwidth, 900,
		XmNheight, 700,
		XmNx, 0,
		XmNy, 0,
	NULL);
	
}
