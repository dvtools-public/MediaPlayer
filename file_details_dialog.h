void trimHomeDirFromPath(const char *path, char *output, size_t output_size)
{
	const char *homeDir = getenv("HOME");
	if (homeDir && strncmp(path, homeDir, strlen(homeDir)) == 0)
	{
		snprintf(output, output_size, "~%s", path + strlen(homeDir));
	}
	else
	{
		snprintf(output, output_size, "%s", path);
	}
}


void makeFileSizeHumanReadable(int64_t size, char *output, size_t output_size)
{
	if(size < 1024)
	{
		snprintf(output, output_size, "%" PRId64 " bytes", size);
	}
	
	else if(size < 1024 * 1024)
	{
		snprintf(output, output_size, "%.2fK", size / 1024.0);
	}
	
	else if(size < 1024 * 1024 * 1024)
	{
		snprintf(output, output_size, "%.2fM", size / (1024.0 * 1024));
	}
	
	else
	{
		snprintf(output, output_size, "%.2fG", size / (1024.0 * 1024 * 1024));
	}
}

void detailsDestroyCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	detailsDialogIsOpen = 0;
	
	XtVaSetValues(fileDetails,
		XmNsensitive, 1,
	NULL);
}

void createFileDetailsDialog(Widget widget, XtPointer client_data, XtPointer call_data)
{
	if(detailsDialogIsOpen)
	{
		return;
	}
	
	detailsDialogIsOpen = 1;
	
	XtVaSetValues(fileDetails,
		XmNsensitive, 0,
	NULL);
	
	
/* initialize a bare dialog shell */
	Widget detailsDialogShell = XmCreateDialogShell(topLevel, "detailsDialogShell", NULL, 0);
	XtVaSetValues(detailsDialogShell,
		XmNtitle, "File Details",
	NULL);
	
	XtAddCallback(detailsDialogShell, XmNpopdownCallback, detailsDestroyCallback, NULL);
	
/* set normal window controls for this dialog */
	int windowDecorations;
	XtVaGetValues(detailsDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
		windowDecorations &= ~MWM_DECOR_TITLE;
		windowDecorations &= ~MWM_DECOR_BORDER;
		windowDecorations &= ~MWM_DECOR_RESIZEH;
	XtVaSetValues(detailsDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	
/* create a form to hold the other widgets */
	Widget infoDialogForm = XtVaCreateManagedWidget("infoDialogForm", xmFormWidgetClass, detailsDialogShell,
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
	NULL);
	
/* cancels the dialog */
	Widget infoCancel = XtVaCreateManagedWidget("infoCancel", xmPushButtonWidgetClass, infoDialogForm,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Dismiss", 4,
	NULL);
	
/* general frame */
	Widget detailsLabelFrame0 = XtVaCreateManagedWidget("detailsLabelFrame0", xmFrameWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		
		XmNshadowType, XmSHADOW_ETCHED_IN,
		XmNshadowThickness, 2,
	NULL);
	
	XtVaCreateManagedWidget("Overview", xmLabelWidgetClass, detailsLabelFrame0,
		XmNchildType, XmFRAME_TITLE_CHILD,
		XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
		XmNmarginHeight, 1,
		XmNmarginWidth, 4,
	NULL);
	
	Widget genForm = XtVaCreateManagedWidget("genForm", xmFormWidgetClass, detailsLabelFrame0,
		XmNshadowThickness, 0,
	NULL);
	
	char genLabel0Strings[8192];
	snprintf(genLabel0Strings, sizeof(genLabel0Strings), "%s%s%s%s%s",
	
		"File Location:\n",
		"Media Title:\n",
		"Duration:\n",
		"Format:\n",
		"Size:\n",
		
	NULL);
	
	
	Widget genLabel0 = XtVaCreateManagedWidget("genLabel0", xmLabelWidgetClass, genForm,
		XmNalignment, XmALIGNMENT_END,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		
		XmNmarginLeft, 8,
		
		XtVaTypedArg, XmNlabelString, XmRString, genLabel0Strings, 4,
	NULL);
	
	Widget genLabel1 = XtVaCreateManagedWidget("genLabel1", xmLabelWidgetClass, genForm,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, genLabel0,
		XmNleftOffset, 0,
		
		XmNmarginLeft, 1,
	NULL);
	
	
	
	
/* audio frame */
	Widget detailsLabelFrame1 = XtVaCreateManagedWidget("detailsLabelFrame1", xmFrameWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, detailsLabelFrame0,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		
		XmNshadowType, XmSHADOW_ETCHED_IN,
		XmNshadowThickness, 2,
	NULL);
	
	XtVaCreateManagedWidget ("AudioDetails", xmLabelWidgetClass, detailsLabelFrame1,
		XmNchildType, XmFRAME_TITLE_CHILD,
		XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
		XtVaTypedArg, XmNlabelString, XmRString, "Audio", 4,
	NULL);
	
	Widget audioForm = XtVaCreateManagedWidget("audioForm", xmFormWidgetClass, detailsLabelFrame1,
		XmNshadowThickness, 0,
	NULL);
	
	
	char audioLabel0Strings[8192];
	snprintf(audioLabel0Strings, sizeof(audioLabel0Strings), "%s%s%s%s%s",
	
		"Format:\n",
		"Codec:\n",
		"Bitrate:\n",
		"Samplerate:\n",
		"Channels:\n",
		
	NULL);
	
	
	Widget audioLabel0 = XtVaCreateManagedWidget("audioLabel0", xmLabelWidgetClass, audioForm,
		XmNalignment, XmALIGNMENT_END,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		
		XmNmarginLeft, 8,
		
		XtVaTypedArg, XmNlabelString, XmRString, audioLabel0Strings, 4,
	NULL);
	
	Widget audioLabel1 = XtVaCreateManagedWidget("audioLabel1", xmLabelWidgetClass, audioForm,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, audioLabel0,
		XmNleftOffset, 0,
		
		XmNmarginLeft, 1,
	NULL);
	
	
/* video frame */
	Widget detailsLabelFrame2 = XtVaCreateManagedWidget("detailsLabelFrame2", xmFrameWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, detailsLabelFrame1,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, infoCancel,
		XmNbottomOffset, 6,
		
		XmNshadowType, XmSHADOW_ETCHED_IN,
		XmNshadowThickness, 2,
	NULL);
	
	XtVaCreateManagedWidget ("Video", xmLabelWidgetClass, detailsLabelFrame2,
		XmNchildType, XmFRAME_TITLE_CHILD,
		XmNchildVerticalAlignment, XmALIGNMENT_CENTER,
	NULL);
	
	
	
	
	
	
	
	
	Dimension buttonHeight;
	XtVaGetValues(infoCancel, XmNheight, &buttonHeight, NULL);
	
	int finalWidth, finalHeight;
	
	finalWidth = 520;
	finalHeight = 500 + buttonHeight;
	

	

/* get _NET_FRAME_EXTENTS for window border and title bar of the dialog shell */
	int borderTop, borderLeft, borderRight, borderBottom;
	int *extents = getInfoDialogFrameExtents(topLevel);
	if(extents != NULL)
	{
		borderLeft = extents[0];
		borderRight = extents[1];
		borderTop = extents[2];
		borderBottom = extents[3];
	}
	
	
/* figure out where top level window is positioned */
	Dimension topShellW, topShellH, topShellX, topShellY;
	XtVaGetValues(topLevel,
		XmNwidth, &topShellW,
		XmNheight, &topShellH,
		XmNx, &topShellX,
		XmNy, &topShellY,
	NULL);
	
	
	int newX, newY;
	newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
	newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderTop;
	
	
	/* set window geometry and position */
	XtVaSetValues(detailsDialogShell,
		XmNwidth, finalWidth, 
		XmNheight, finalHeight,
		XmNx, newX,
		XmNy, newY,
	NULL);
	
	
/* give dismiss button default focus */
	XtAddEventHandler(detailsDialogShell, ExposureMask, False, 
	(XtEventHandler)activeDialog, (XtPointer)infoCancel);
	
	
	
	mpv_handle *mpv = (mpv_handle *)client_data;

	char buffer0[8192];
	char temp0[8192];

	const char *file_path = NULL;
	const char *media_title = NULL;
	double duration = 0.0;
	const char *file_format = NULL;
	int64_t file_size = 0;
	const char *audio_format = NULL;
	const char *audio_codec = NULL;
	double audio_bitrate = 0.0;
	int64_t audio_samplerate = 0;
	int64_t audio_channels = 0;
	int64_t width = 0;
	int64_t height = 0;
	const char *video_format = NULL;
	const char *video_codec = NULL;
	double video_bitrate = 0.0;
	double video_fps = 0.0;

	buffer0[0] = '\0'; // Initialize buffer

/* file location */
	if (mpv_get_property(mpv, "path", MPV_FORMAT_STRING, &file_path) >= 0)
	{
		trimHomeDirFromPath(file_path, temp0, sizeof(temp0));
		strncat(buffer0, temp0, sizeof(buffer0) - strlen(buffer0) - 1);
		strncat(buffer0, "\n", sizeof(buffer0) - strlen(buffer0) - 1);
		mpv_free((void *)file_path);
	}
	else
	{
		strncat(buffer0, "N/A\n", sizeof(buffer0) - strlen(buffer0) - 1);
	}
	
	
/* embedded title */
	if (mpv_get_property(mpv, "media-title", MPV_FORMAT_STRING, &media_title) >= 0)
	{
		snprintf(temp0, sizeof(temp0), "%s\n", media_title);
		strncat(buffer0, temp0, sizeof(buffer0) - strlen(buffer0) - 1);
		mpv_free((void *)media_title);
	}
	else
	{
		strncat(buffer0, "N/A\n", sizeof(buffer0) - strlen(buffer0) - 1);
	}
	
	
/* duration */
	if (mpv_get_property(mpv, "duration", MPV_FORMAT_DOUBLE, &duration) >= 0)
	{
		snprintf(temp0, sizeof(temp0), "%.2f seconds\n", duration);
		strncat(buffer0, temp0, sizeof(buffer0) - strlen(buffer0) - 1);
	}
	else
	{
		strncat(buffer0, "N/A\n", sizeof(buffer0) - strlen(buffer0) - 1);
	}
	

/* format */
	if (mpv_get_property(mpv, "file-format", MPV_FORMAT_STRING, &file_format) >= 0)
	{
		snprintf(temp0, sizeof(temp0), "%s\n", file_format);
		strncat(buffer0, temp0, sizeof(buffer0) - strlen(buffer0) - 1);
		mpv_free((void *)file_format);
	}
	else
	{
		strncat(buffer0, "N/A\n", sizeof(buffer0) - strlen(buffer0) - 1);
	}
	
	
/* file size */
	if (mpv_get_property(mpv, "file-size", MPV_FORMAT_INT64, &file_size) >= 0)
	{
		
		makeFileSizeHumanReadable(file_size, temp0, sizeof(temp0));
		strncat(buffer0, temp0, sizeof(buffer0) - strlen(buffer0) - 1);
		strncat(buffer0, "\n", sizeof(buffer0) - strlen(buffer0) - 1);
	}
	else
	{
		strncat(buffer0, "N/A\n", sizeof(buffer0) - strlen(buffer0) - 1);
	}


	XtVaSetValues(genLabel1,
		XmNlabelString, XmStringCreateLtoR(buffer0, XmFONTLIST_DEFAULT_TAG),
	NULL);
	
	
	
	
	
	printf("\nAudio:\n");

	if (mpv_get_property(mpv, "audio-format", MPV_FORMAT_STRING, &audio_format) >= 0)
	{
		printf("  * Audio Format: %s\n", audio_format);
		mpv_free((void *)audio_format);
	}
	else
	{
		printf("  * Audio Format: N/A\n");
	}

	if (mpv_get_property(mpv, "audio-codec", MPV_FORMAT_STRING, &audio_codec) >= 0)
	{
		printf("  * Audio Codec: %s\n", audio_codec);
		mpv_free((void *)audio_codec);
	}
	else
	{
		printf("  * Audio Codec: N/A\n");
	}

	if (mpv_get_property(mpv, "audio-bitrate", MPV_FORMAT_DOUBLE, &audio_bitrate) >= 0)
	{
		printf("  * Audio Bitrate: %.2f kbps\n", audio_bitrate);
	}
	else
	{
		printf("  * Audio Bitrate: N/A\n");
	}

	if (mpv_get_property(mpv, "audio-samplerate", MPV_FORMAT_INT64, &audio_samplerate) >= 0)
	{
		printf("  * Audio Samplerate: %" PRId64 " Hz\n", audio_samplerate);
	}
	else
	{
		printf("  * Audio Samplerate: N/A\n");
	}

	if (mpv_get_property(mpv, "audio-channels", MPV_FORMAT_INT64, &audio_channels) >= 0)
	{
		printf("  * Audio Channels: %" PRId64 "\n", audio_channels);
	}
	else
	{
		printf("  * Audio Channels: N/A\n");
	}

	printf("\nVideo:\n");

	if (mpv_get_property(mpv, "width", MPV_FORMAT_INT64, &width) >= 0)
	{
		printf("  * Width: %" PRId64 " px\n", width);
	}
	else
	{
		printf("  * Width: N/A\n");
	}

	if (mpv_get_property(mpv, "height", MPV_FORMAT_INT64, &height) >= 0)
	{
		printf("  * Height: %" PRId64 " px\n", height);
	}
	else
	{
		printf("  * Height: N/A\n");
	}

	if (mpv_get_property(mpv, "video-format", MPV_FORMAT_STRING, &video_format) >= 0)
	{
		printf("  * Video Format: %s\n", video_format);
		mpv_free((void *)video_format);
	}
	else
	{
		printf("  * Video Format: N/A\n");
	}

	if (mpv_get_property(mpv, "video-codec", MPV_FORMAT_STRING, &video_codec) >= 0)
	{
		printf("  * Video Codec: %s\n", video_codec);
		mpv_free((void *)video_codec);
	}
	else
	{
		printf("  * Video Codec: N/A\n");
	}

	if (mpv_get_property(mpv, "video-bitrate", MPV_FORMAT_DOUBLE, &video_bitrate) >= 0)
	{
		printf("  * Video Bitrate: %.2f kbps\n", video_bitrate);
	}
	else
	{
		printf("  * Video Bitrate: N/A\n");
	}

	if (mpv_get_property(mpv, "video-fps", MPV_FORMAT_DOUBLE, &video_fps) >= 0)
	{
		printf("  * Video FPS: %.2f\n", video_fps);
	}
	else
	{
		printf("  * Video FPS: N/A\n");
	}
	
	
}



