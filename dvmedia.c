/* system/POSIX header files */
#include <stdio.h>
#include <stdlib.h> /* system() */
#include <string.h> /* strcpy() */
#include <unistd.h> /* access() */
#include <sys/types.h>
#include <sys/stat.h> /* stat() */
#include <pthread.h> /* POSIX multi-threading */
#include <time.h> /* time(), strftime() */
#include <inttypes.h>
#include <locale.h>

#include <Xm/Xm.h> /* motif core */
#include <Xm/MainW.h> /* high level manager form within topLevel */
#include <Xm/RowColumn.h> /* menu bar */
#include <Xm/CascadeB.h> /* cascading menus in menuBar */
#include <Xm/PushB.h> /* buttons in menus and main form */
#include <Xm/ToggleB.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/Scale.h>
#include <Xm/Text.h> /* text input boxes */
#include <Xm/Separator.h>
#include <Xm/Protocols.h> /* keyboard shortcuts */
#include <Xm/ScrolledW.h> /* holds left disk list radios */
#include <Xm/XmStrDefs.h>
#include <Xm/DialogS.h>
#include <Xm/MwmUtil.h>
#include <Xm/FileSB.h>


#include <X11/Xlib.h>
#include <X11/Intrinsic.h> /* XInitThreads() for X11 multi-tthreading */
#include <X11/Shell.h>
#include <X11/Xatom.h> /* get motif WM properties */
#include <X11/extensions/Xinerama.h> /* window centering */
#include <X11/xpm.h> /* pixmaps */

#include <mpv/client.h>
#include <mpv/render_gl.h>

const char *versionString = "0.8.2";
const char *dateString = "July 6th, 2024";
char libmpvVersion[32] = "\0";
char hasHardwareAccel[32] = "\0";
char videoOutputDev[32] = "\0";

typedef struct
{
	mpv_handle *mpv;
	Widget scale;
	Widget label;
} AppData;


int centeredWindowFirstRun = 0;

char fileInputString[8192];
int openedWithFile = 1;
int fileIsLoaded = 0;
int isPlaying = 1;
int scaleUpdate_msec = 100; /* anything below 25ms causes label flickering */
int controlAreaVisible = 1;
int percBarVisible = 0;
int playingSpeed = 0;
double seekSeconds = 10;
double resetPosition = 0;
int infLoop = 0;
int wasReset = 0;
int endOfFile = 0;
int isMuted = 0;

int selectionDialogIsOpen = 0;
int detailsDialogIsOpen = 0;
int volumeDialogIsOpen = 0;

/* define xt context */
	XtAppContext app;
	
/* make widgets globally accessible */
Widget topLevel, mainWin,
menuBar,
	fileMenuPane, 
		fileOpen,
			fileSelectionDialogShell,
			fileSelectionBox,
			okButton,
		fileDetails,
	pbMenuPane,
		togglePlaybackButton,
		infiniteLoop,
		playbackSpeed,
			speedValue0,
			speedValue1,
			speedValue2,
			speedValue3,
			speedValue4,
			speedValue5,
			speedValue6,
			speedValue7,
			speedValue8,
			speedValue9,
		seekBackward,
		seekForward,
		seekValue,
			seekValue0,
			seekValue1,
			seekValue2,
			seekValue3,
			seekValue4,
			seekValue5,
			seekValue6,
			seekValue7,
		resetReload,
	optMenuPane,
		optEntry0,
	audioMenuPane,
		audioEntry0,
		audioEntry1,
			outputAll,
			outputLeft,
			outputRight,
		audioEntry2,
		audioEntry3,
	videoMenuPane,
		videoEntry0,
			fullscreenVideoShell,
		videoEntry1,
		videoEntry2,
	winMenuPane, 
		winEntry0,
		winEntry1,
		winEntry2,
		winEntry3,
masterForm, 
	videoForm,
	sliderForm,
	positionSlider,
	sliderJumpButtonContainer,
		percBar0,
		percBar1,
		percBar2,
		percBar3,
		percBar4,
		percBar5,
		percBar6,
		percBar7,
		percBar8,
		percBar9,
	controlButtonContainer,

controlForm,
	pauseButton,
	popupPauseButton,
	progressLabel,
	statusLabel,
	statusLabel1,
	statusLabel2;

Pixmap pause_pixmap, resume_pixmap,
	mute0, mute1,
	play_once, inf_loop;
	
Pixel mainFG, mainBG;

mpv_handle *mpv;


#include "info_dialog.h" /* popup dialogs for about/help */
#include "quick_dialog.h"

#include "file_selection_dialog.h"
#include "file_details_dialog.h"
#include "volume_dialog.h"
#include "fullscreen.h"

#include "icon_data.h" /* custom bitmaps */
#include "callbacks.h" /* callbacks for widgets */
#include "jump_bar.h"
#include "menus.h" /* create menubar entries */
#include "xresources.h" /* fallback X11 resources */


int main(int argc, char *argv[])
{
/* send annoying widget warnings to the null device */
	freopen("/dev/null", "w", stderr);
	
/* initialize x11 multi-threading - required to XFlush from pthreads */
	XInitThreads();
	
/* required for picking up non-english chars in the file picker selection
   i.e. german or swedish 'a' with umlaut/overring
*/
	setlocale(LC_CTYPE, "");
	


	if(argc < 2) /* if no user flags were passed */
	{
		openedWithFile = 0; /* no file path or URL was provided */
	}
	
	else if(argv[1][0] == '-') /* a user flag starting with '-' was passed */
	{
		openedWithFile = 0;
		
		/* handle help flags */
		if(strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "-help") == 0 || strcmp(argv[1], "--help") == 0)
		{
			printf("\n help flag found \n\n");
			fflush(stdout);
			
			/* exit after help text prints */
			exit(0);
		}
        }
	
	else for(int i = 1; i < argc; ++i) /* an argument was passed that does no begin with '-' */
	{
		openedWithFile = 1; /* assumes file path or URL */
		
		/* max path length is something like 4096 and the max filename 
		   length is 255 so a buffer size of 8192 should be sufficient
		*/
		if(strlen(fileInputString) + strlen(argv[i]) < sizeof(fileInputString))
		{
			strcat(fileInputString, argv[i]);
			
			if(i < argc - 1)
			{
				strcat(fileInputString, " "); /* add spaces */
			}
		}
	}
	
	
	topLevel = XtVaAppInitialize(&app, "dvmedia", NULL, 0, &argc, argv, fb_xres, NULL);
	/* set some initial resources for topLevel*/
	XtVaSetValues(topLevel,
		XmNtitle, "Media Player",
		XmNiconName, " Media Player ",
		
		XmNwidth, 640,
		XmNheight, 480,
		/*
		XmNtoolTipEnable, 1,
		XmNtoolTipPostDelay, 5000,
		XmNtoolTipPostDuration, 0,
		*/
	NULL);
	
	
	
	int windowDecorations;
	XtVaGetValues(topLevel, XmNmwmDecorations, &windowDecorations, NULL);
		windowDecorations &= ~MWM_DECOR_MENU;
		windowDecorations &= ~MWM_DECOR_TITLE;
		windowDecorations &= ~MWM_DECOR_MINIMIZE;
		windowDecorations &= ~MWM_DECOR_MAXIMIZE;
		windowDecorations &= ~MWM_DECOR_BORDER;
		windowDecorations &= ~MWM_DECOR_RESIZEH;
	XtVaSetValues(topLevel, XmNmwmDecorations, windowDecorations, NULL);
	
/* XmMainWindow as high level container widget */
	mainWin = XtVaCreateManagedWidget("mainWin", xmMainWindowWidgetClass, topLevel, NULL);
	XtVaSetValues(mainWin,
		XmNshadowThickness, 0,
	NULL);
	
/* create a menu bar by brute forcing a XmRowColumn widget with XmNrowColumnType */
	menuBar = XtVaCreateManagedWidget("menuBar", xmRowColumnWidgetClass, mainWin, 
		XmNrowColumnType, XmMENU_BAR,
		XmNshadowThickness, 1,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNmarginHeight, 4,
	NULL);

/* split off into menus.h */
	installMenus();

/* create another container form below the menu bar */
	masterForm = XtVaCreateManagedWidget("masterForm", xmFormWidgetClass, mainWin,
		XmNshadowThickness, 0,
		XmNshadowType, XmSHADOW_IN,
		/* pin the main form underneath the menu bar */
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, menuBar,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XtVaTypedArg, XmNbackground, XmRString, "black", 4,
	NULL);
	
	controlForm = XtVaCreateManagedWidget("controlForm", xmFormWidgetClass, masterForm,
		XmNshadowThickness, 1,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
	NULL);
	
	videoForm = XtVaCreateManagedWidget("masterForm", xmFormWidgetClass, masterForm,
		XmNshadowThickness, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 5,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 5,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 5,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, controlForm,
		XmNbottomOffset, 5,
		
		XtVaTypedArg, XmNbackground, XmRString, "black", 4,
	NULL);
	
	
	XtVaGetValues(masterForm, XmNforeground, &mainFG, NULL);
	
	sliderForm = XtVaCreateManagedWidget("sliderForm", xmFormWidgetClass, controlForm,
		XmNshadowThickness, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
	NULL);
	
	positionSlider = XtVaCreateManagedWidget("positionSlider", xmScaleWidgetClass, sliderForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 1,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 1,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 1,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNminimum, 0,
		XmNmaximum, 1000,
		
		XmNshowValue, 0,
		XmNhighlightThickness, 0,
		XmNtraversalOn, 0,
		
		XmNorientation, XmHORIZONTAL,
	NULL);
	
	sliderJumpButtonContainer = XtVaCreateManagedWidget("sliderJumpButtonContainer", xmFormWidgetClass, controlForm,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sliderForm,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		
		XmNfractionBase, 100,
		XmNshadowThickness, 0,
		
		XmNbackground, mainFG,
		XmNmappedWhenManaged, 0,
	NULL);
	
	
	
	controlButtonContainer = XtVaCreateManagedWidget("controlButtonContainer", xmFormWidgetClass, controlForm,
		XmNshadowThickness, 0,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sliderForm,
		XmNtopOffset, 5,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 9,
	NULL);
	
/* this is here so that the icons have a solid background to hide behind when topLevel is shrunk down to minWidth */
	Widget secondaryContainer = XtVaCreateManagedWidget("sliderForm", xmFormWidgetClass, controlButtonContainer,
		XmNshadowThickness, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
	NULL);
	
	/* skip left pixmap */
	Pixmap skip_left_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), skip_left_bits, skip_left_width, skip_left_height);
	
	/*
	XmString tooltipString = XmStringCreateLocalized("tooltip");
	*/
	Widget skipLeft = XtVaCreateManagedWidget("skipLeft", xmPushButtonWidgetClass, secondaryContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNlabelType, XmPIXMAP,
		XmNarmPixmap, skip_left_pixmap,
		
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XmNtraversalOn, 0,
		/*
		XmNtoolTipString, tooltipString,
		*/
	NULL);
	/*
	XmStringFree(tooltipString);
	*/
	
	/* pause button pixmap */
	pause_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), pause_bits, pause_width, pause_height);
	
	/* resume button pixmap */
	resume_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), resume_bits, resume_width, resume_height);
	
	pauseButton = XtVaCreateManagedWidget("pauseButton", xmPushButtonWidgetClass, secondaryContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, skipLeft,
		XmNleftOffset, 5,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNlabelType, XmPIXMAP,
		XmNlabelPixmap, pause_pixmap,
		
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
	NULL);
	
	/* skip right pixmap */
	Pixmap skip_right_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), skip_right_bits, skip_right_width, skip_right_height);
	
	Widget skipRight = XtVaCreateManagedWidget("skipRight", xmPushButtonWidgetClass, secondaryContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, pauseButton,
		XmNleftOffset, 5,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNlabelType, XmPIXMAP,
		XmNlabelPixmap, skip_right_pixmap,
		
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XmNtraversalOn, 0,
	NULL);
	
	/* restart button pixmap */
	Pixmap restart_pixmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), restart_bits, restart_width, restart_height);
	
	Widget restartButton = XtVaCreateManagedWidget("restartButton", xmPushButtonWidgetClass, secondaryContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, skipRight,
		XmNleftOffset, 5,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNlabelType, XmPIXMAP,
		XmNarmPixmap, restart_pixmap,
		
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		
		XmNtraversalOn, 0,
	NULL);
	
	progressLabel = XtVaCreateManagedWidget("progressLabel", xmLabelWidgetClass, controlButtonContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNalignment, XmALIGNMENT_END,
		
		XtVaTypedArg, XmNlabelString, XmRString, "00:00:00 / 00:00:00", 4,
		
		XmNmarginHeight, 4,
		XmNmarginLeft, 1,
		XmNmarginRight, 7,
	NULL);
	
	Widget statusSep0 = XtVaCreateManagedWidget("statusSep0", xmLabelWidgetClass, controlButtonContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, progressLabel,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		XtVaTypedArg, XmNlabelString, XmRString, "-", 4,
	NULL);
	
/* playback status */
	statusLabel = XtVaCreateManagedWidget("statusLabel", xmLabelWidgetClass, controlButtonContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, statusSep0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNalignment, XmALIGNMENT_END,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Playing", 4,
		
		XmNmarginHeight, 4,
		XmNmarginLeft, 1,
		XmNmarginRight, 1,
	NULL);
	
	
	/* play once pixmap */
	play_once = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), play_once_bits, play_once_width, play_once_height);
	
	/* infinite loop pixmap */
	inf_loop = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), inf_loop_bits, inf_loop_width, inf_loop_height);
	
	
/* infinite loop status */
	statusLabel1 = XtVaCreateManagedWidget("statusLabel1", xmLabelWidgetClass, controlButtonContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, statusLabel,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNalignment, XmALIGNMENT_CENTER,
		XmNlabelType, XmPIXMAP,
		XmNlabelPixmap, play_once,
		
		XmNmarginHeight, 4,
		XmNmarginLeft, 1,
		XmNmarginRight, 6,
		
		/*
		XtVaTypedArg, XmNbackground, XmRString, "red", 4,
		*/
	NULL);
	
	
	
	/* unmuted pixmap */
	mute0 = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), mute_0_bits, mute_0_width, mute_0_height);
	
	/* muted pixmap */
	mute1 = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), mute_1_bits, mute_1_width, mute_1_height);
	
/* mute status */
	statusLabel2 = XtVaCreateManagedWidget("statusLabel2", xmLabelWidgetClass, controlButtonContainer,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, statusLabel1,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNalignment, XmALIGNMENT_CENTER,
		XmNlabelType, XmPIXMAP,
		XmNlabelPixmap, mute0,
		
		XmNmarginHeight, 4,
		XmNmarginLeft, 1,
		XmNmarginRight, 2,
		
		/*
		XtVaTypedArg, XmNbackground, XmRString, "blue", 4,
		*/
	NULL);
	
	
	
/*
	For some stupid reason the right click callback only works when there's a popup menu inside videoForm.
	This widget gets unmapped on startup and does nothing. It shouldn't hurt anything.
*/
	Widget rightClickFix = XmCreatePopupMenu(videoForm, "rightClickFix", NULL, 0);
	XtVaSetValues(rightClickFix,
		XmNmappedWhenManaged, 0,
	NULL);
	
	
	
	/* close top level */
	XtRealizeWidget(topLevel);
	
	
	
/* center window on first monitor */
	XtAddEventHandler(topLevel, StructureNotifyMask, False, centerWindow, NULL);
	
	Display *display = XtDisplay(topLevel);
	Window window = XtWindow(topLevel);
	
/* leftover debug code saved for later - find out if topLevel is mapped and visible */
/*
	XWindowAttributes attrs;
	XGetWindowAttributes(display, window, &attrs);
	if(attrs.map_state != IsViewable)
	{
		printf("Window is not viewable or mapped.\n");
	}
	else
	{
		printf("Window is viewable and mapped.\n");
	}
*/
	
	
	/* Initialize mpv */
	Window videoWindow = XtWindow(videoForm);
	
	/* mpv_handle *mpv; */
	
	mpv = mpv_create();
	
	setup_mpv(mpv, videoWindow);
	
	createPercBar(sliderJumpButtonContainer);
	XtAddCallback(percBar0, XmNactivateCallback, percBar0cb, (XtPointer)mpv);
	XtAddCallback(percBar1, XmNactivateCallback, percBar1cb, (XtPointer)mpv);
	XtAddCallback(percBar2, XmNactivateCallback, percBar2cb, (XtPointer)mpv);
	XtAddCallback(percBar3, XmNactivateCallback, percBar3cb, (XtPointer)mpv);
	XtAddCallback(percBar4, XmNactivateCallback, percBar4cb, (XtPointer)mpv);
	XtAddCallback(percBar5, XmNactivateCallback, percBar5cb, (XtPointer)mpv);
	XtAddCallback(percBar6, XmNactivateCallback, percBar6cb, (XtPointer)mpv);
	XtAddCallback(percBar7, XmNactivateCallback, percBar7cb, (XtPointer)mpv);
	XtAddCallback(percBar8, XmNactivateCallback, percBar8cb, (XtPointer)mpv);
	XtAddCallback(percBar9, XmNactivateCallback, percBar9cb, (XtPointer)mpv);
	
	
	/*
	XtAddCallback(skipLeft, XmNactivateCallback, seekLeft, (XtPointer)mpv);
	XtAddCallback(skipRight, XmNactivateCallback, seekRight, (XtPointer)mpv);
	*/
	
	XtAddCallback(skipLeft, XmNactivateCallback, seekLeft, (XtPointer)mpv);
	XtAddCallback(pauseButton, XmNactivateCallback, togglePlayback, (XtPointer)mpv);
	XtAddCallback(skipRight, XmNactivateCallback, seekRight, (XtPointer)mpv);
	XtAddCallback(restartButton, XmNactivateCallback, resetCallback, (XtPointer)mpv);
	
	
	
	AppData app_data;
	app_data.mpv = mpv;
	app_data.scale = positionSlider;
	app_data.label = progressLabel;
	
	
	XtAppAddTimeOut(XtWidgetToApplicationContext(positionSlider), 10, update_scale, (XtPointer)&app_data);
	
	XtAddCallback(positionSlider, XmNdragCallback, scaleCallback, (XtPointer)&app_data);
	XtAddCallback(positionSlider, XmNvalueChangedCallback, scaleCallback, (XtPointer)&app_data);
	
	
	/* event handler for right clicks */
	XtAddEventHandler(videoForm, ButtonPressMask, False, rightClickCallback, (XtPointer)&app_data);
	
	
	/* menu bar callbacks */
	XtAddCallback(fileDetails, XmNactivateCallback, createFileDetailsDialog, (XtPointer)mpv);
	XtAddCallback(togglePlaybackButton, XmNactivateCallback, togglePlayback, (XtPointer)mpv);
	
	XtAddCallback(infiniteLoop, XmNactivateCallback, toggleInfiniteLoop, (XtPointer)mpv);
	
	XtAddCallback(seekBackward, XmNactivateCallback, seekLeft, (XtPointer)mpv);
	XtAddCallback(seekForward, XmNactivateCallback, seekRight, (XtPointer)mpv);
	XtAddCallback(resetReload, XmNactivateCallback, resetCallback, (XtPointer)mpv);
	
	XtAddCallback(audioEntry0, XmNactivateCallback, toggleMute, (XtPointer)mpv);
	
	
	/*
	XtAddCallback(okButton, XmNactivateCallback, okCallback, (XtPointer)&app_data);
	*/
	XtAddCallback(fileOpen, XmNactivateCallback, createTemporaryFilePickerDialog, (XtPointer)&app_data);
	
	XtAddCallback(speedValue0, XmNvalueChangedCallback, speedCallback0, NULL);
	XtAddCallback(speedValue1, XmNvalueChangedCallback, speedCallback1, NULL);
	XtAddCallback(speedValue2, XmNvalueChangedCallback, speedCallback2, NULL);
	XtAddCallback(speedValue3, XmNvalueChangedCallback, speedCallback3, NULL);
	XtAddCallback(speedValue4, XmNvalueChangedCallback, speedCallback4, NULL);
	XtAddCallback(speedValue5, XmNvalueChangedCallback, speedCallback5, NULL);
	XtAddCallback(speedValue6, XmNvalueChangedCallback, speedCallback6, NULL);
	XtAddCallback(speedValue7, XmNvalueChangedCallback, speedCallback7, NULL);
	XtAddCallback(speedValue8, XmNvalueChangedCallback, speedCallback8, NULL);
	XtAddCallback(speedValue9, XmNvalueChangedCallback, speedCallback9, NULL);
	
	
	XtAddCallback(seekValue0, XmNvalueChangedCallback, seekTimeCallback0, NULL);
	XtAddCallback(seekValue1, XmNvalueChangedCallback, seekTimeCallback1, NULL);
	XtAddCallback(seekValue2, XmNvalueChangedCallback, seekTimeCallback2, NULL);
	XtAddCallback(seekValue3, XmNvalueChangedCallback, seekTimeCallback3, NULL);
	XtAddCallback(seekValue4, XmNvalueChangedCallback, seekTimeCallback4, NULL);
	XtAddCallback(seekValue5, XmNvalueChangedCallback, seekTimeCallback5, NULL);
	XtAddCallback(seekValue6, XmNvalueChangedCallback, seekTimeCallback6, NULL);
	XtAddCallback(seekValue7, XmNvalueChangedCallback, seekTimeCallback7, NULL);
	
	XtAddCallback(audioEntry3, XmNactivateCallback, createVolumeDialog, (XtPointer)mpv);
	
	
	Dimension menuBar_h, controlForm_h;
	XtVaGetValues(menuBar, XmNheight, &menuBar_h, NULL);
	XtVaGetValues(controlForm, XmNheight, &controlForm_h, NULL);
	
	Dimension secondaryContainer_w, progressLabel_w, statusSep0_w, statusLabel_w;
	XtVaGetValues(secondaryContainer, XmNwidth, &secondaryContainer_w, NULL);
	XtVaGetValues(progressLabel, XmNwidth, &progressLabel_w, NULL);
	XtVaGetValues(statusSep0, XmNwidth, &statusSep0_w, NULL);
	XtVaGetValues(statusLabel, XmNwidth, &statusLabel_w, NULL);
	
	XtVaSetValues(topLevel,
		XmNminHeight, menuBar_h + controlForm_h,
		XmNminWidth, secondaryContainer_w + progressLabel_w + statusLabel_w + 40, /* 40px accounts for offsets */
	NULL);
	
/* enter main processing loop */
	XtAppMainLoop(app);
	
	mpv_terminate_destroy(mpv);
	
	return 0;
}





