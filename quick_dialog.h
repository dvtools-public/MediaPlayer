/* 
	A big pile of spaghetti that I won't untangle anytime soon. 
	It's all part of the dog's dinner now.
*/


void createQuickErrorDialog(char *winTitle, char *msgString)
{
	int icon = 2;
	int frame = 0;
	
	
/* initialize a bare dialog shell */
	Widget infoDialogShell = XmCreateDialogShell(topLevel, "infoDialogShell", NULL, 0);
	XtVaSetValues(infoDialogShell,
		XmNtitle, winTitle,
	NULL);
	
	
/* tell vendorshell what kind of window decorations to apply to the dialog */
	int windowDecorations;
		
	if(frame == 0) /* no border */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= 0;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
	else if(frame == 1) /* outer border frame only */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= ~MWM_DECOR_BORDER;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
	else if(frame == 2) /* title + outer border */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= ~MWM_DECOR_TITLE;
			windowDecorations &= ~MWM_DECOR_BORDER;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
	else if(frame == 3) /* menu + title + outer border */
	{
		XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
			windowDecorations &= ~MWM_DECOR_MENU;
			windowDecorations &= ~MWM_DECOR_TITLE;
			windowDecorations &= ~MWM_DECOR_BORDER;
		XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	}
	
/* create a form to hold the other widgets */
	Widget infoDialogForm = XtVaCreateManagedWidget("infoDialogForm", xmFormWidgetClass, infoDialogShell,
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
	NULL);
	
/* cancels the dialog */
	Widget infoCancel = XtVaCreateManagedWidget("infoCancel", xmPushButtonWidgetClass, infoDialogForm,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Dismiss", 4,
	NULL);
	
/* etched in frame around main message */
	Widget infoLabelFrame = XtVaCreateManagedWidget("infoLabelFrame", xmFrameWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, infoCancel,
		XmNbottomOffset, 6,
		
		XmNshadowType, XmSHADOW_ETCHED_IN,
		XmNshadowThickness, 2,
	NULL);
	
/* main message label */
	Widget infoTextArea = XtVaCreateManagedWidget("infoTextArea", xmTextWidgetClass, infoLabelFrame,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 20,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		
		XmNmarginHeight, 9,
		XmNmarginWidth, 10,
		
		XmNhighlightThickness, 0,
		XmNshadowThickness, 0,
		
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNeditMode, XmSINGLE_LINE_EDIT,
		XmNeditable, 0,
		XmNscrollVertical, 0,
		XmNscrollHorizontal, 0,
		XmNcursorPositionVisible, 0,
		XmNautoShowCursorPosition, 0,
		XmNtraversalOn, 0,
	NULL);
	XmTextSetString(infoTextArea, msgString);
	
	
/* get colors from main dialog XmForm */
	Pixel mainBG, mainFG;
	XtVaGetValues(infoDialogForm,
		XmNbackground, &mainBG,
		XmNforeground, &mainFG,
	NULL);
	
	/* apply colors to XmText widget for consistency */
	XtVaSetValues(infoTextArea,
		XmNbackground, mainBG,
		XmNforeground, mainFG,
	NULL);
	
/* get height/width of text and widgets */
	Dimension textWidth, textHeight, buttonHeight;
	getInfoTextDimensions(infoTextArea, &textWidth, &textHeight);
	XtVaGetValues(infoCancel, XmNheight, &buttonHeight, NULL);
	
	
/* create icon pixmaps */
Pixmap info_pixmap, info_mask;
	XpmCreatePixmapFromData(XtDisplay(infoDialogShell), 
	RootWindowOfScreen(XtScreen(infoDialogShell)), 
	info_xpm, &info_pixmap, &info_mask, NULL);

Pixmap error_pixmap, error_mask;
	XpmCreatePixmapFromData(XtDisplay(infoDialogShell), 
	RootWindowOfScreen(XtScreen(infoDialogShell)), 
	error_xpm, &error_pixmap, &error_mask, NULL);
	
Pixmap warn_pixmap, warn_mask;
	XpmCreatePixmapFromData(XtDisplay(infoDialogShell), 
	RootWindowOfScreen(XtScreen(infoDialogShell)), 
	warn_xpm, &warn_pixmap, &warn_mask, NULL);
	
	
	int finalWidth, finalHeight;
	
/* if icon is enabled */
	if(icon != 0)
	{
		finalWidth = textWidth + 90;
		finalHeight = textHeight + 38 + buttonHeight;
		
		/* create icon frame */
		Widget infoIconFrame = XtVaCreateManagedWidget("infoIconFrame", xmFrameWidgetClass, infoDialogForm,
			XmNtopAttachment, XmATTACH_FORM,
			XmNtopOffset, 8,
			XmNleftAttachment, XmATTACH_FORM,
			XmNleftOffset, 8,
			XmNheight, 42,
			XmNwidth, 42,
			
			XmNshadowType, XmSHADOW_IN,
			XmNshadowThickness, 1,
		NULL);
		
		XtVaSetValues(infoLabelFrame, 
			XmNleftAttachment, XmATTACH_WIDGET,
			XmNleftWidget, infoIconFrame,
			XmNleftOffset, 6,
		NULL);
		
/* set icon type based on argument */
		if(icon == 1) { XtVaSetValues(infoIconFrame, XmNbackgroundPixmap, info_pixmap, NULL); }
		
		if(icon == 2) { XtVaSetValues(infoIconFrame, XmNbackgroundPixmap, error_pixmap, NULL); }
		
		if(icon == 3) { XtVaSetValues(infoIconFrame, XmNbackgroundPixmap, warn_pixmap, NULL); }
	}
	
	if(icon == 0)
	{
		finalWidth = textWidth + 38; /* 38px accounts for widget margins and offsets */
		finalHeight = textHeight + 38 + buttonHeight;
	}
	
/* fixup height values so the frame aligns vertically with the icon frame */
	if(textHeight < 26)
	{
		int alignWithIcon = 26 - textHeight;
		finalHeight = textHeight + 38 + buttonHeight + alignWithIcon;
	}
	

/* get _NET_FRAME_EXTENTS for window border and title bar of the dialog shell */
	int borderTop, borderLeft, borderRight, borderBottom;
	int *extents = getInfoDialogFrameExtents(infoDialogShell);
	if(extents != NULL)
	{
		borderLeft = extents[0];
		borderRight = extents[1];
		borderTop = extents[2];
		borderBottom = extents[3];
	}
	
	
/* figure out where top level window is positioned */
	Dimension topShellW, topShellH, topShellX, topShellY;
	XtVaGetValues(topLevel,
		XmNwidth, &topShellW,
		XmNheight, &topShellH,
		XmNx, &topShellX,
		XmNy, &topShellY,
	NULL);
	
	
/* figure out where dialog shell window is positioned */
	Dimension infoDialogX, infoDialogY;
	XtVaGetValues(infoDialogForm,
		XmNx, &infoDialogX,
		XmNy, &infoDialogY,
	NULL);
	

	int newX, newY;
		
		if(frame == 0 || frame == 1)
		{
			newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
			
			/* -20 compensates for missing titlebar */
			newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderBottom - 20;
		}
		
		else
		{
			newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
			newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderTop;
		}
		
	/* set window geometry and position */
	XtVaSetValues(infoDialogShell,
		XmNwidth, finalWidth, 
		XmNheight, finalHeight,
		XmNx, newX,
		XmNy, newY,
	NULL);
	
	
	
/* give dismiss button default focus */
	XtAddEventHandler(infoDialogShell, ExposureMask, False, 
	(XtEventHandler)activeDialog, (XtPointer)infoCancel);
}


