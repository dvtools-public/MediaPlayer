/* ensure Dismiss gains focus when window is active */
void createColorBars(Widget widget, int barWidth)
{
	Widget colorBarForm = XtVaCreateManagedWidget("colorBarForm", xmFormWidgetClass, widget,
		XmNshadowThickness, 0,
		XmNfractionBase, 100,
		
		XtVaTypedArg, XmNbackground, XmRString, "blue", 4,
	NULL);
	
	Widget colorBarGreen = XtVaCreateManagedWidget("colorBarGreen", xmFrameWidgetClass, colorBarForm,
		XmNtopAttachment, XmATTACH_POSITION,
		XmNtopPosition, 58,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		
		XmNwidth, barWidth,
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#7fc693", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#5b906a", 4,
		XtVaTypedArg, XmNbottomShadowColor, XmRString, "#3d6047", 4,
	NULL);
	
	Widget colorBarYellow = XtVaCreateManagedWidget("colorBarYellow", xmFrameWidgetClass, colorBarForm,
		XmNtopAttachment, XmATTACH_POSITION,
		XmNtopPosition, 24,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_POSITION,
		XmNbottomPosition, 58,
		
		XmNwidth, barWidth,
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#d6c60b", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#ada517", 4,
		XtVaTypedArg, XmNbottomShadowColor, XmRString, "#7e750a", 4,
	NULL);
	
	Widget colorBarRed = XtVaCreateManagedWidget("colorBarRed", xmFrameWidgetClass, colorBarForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_POSITION,
		XmNbottomPosition, 24,
		
		XmNwidth, barWidth,
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_OUT,
		
		XtVaTypedArg, XmNtopShadowColor, XmRString, "#dd6f6c", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#b04744", 4,
		XtVaTypedArg, XmNbottomShadowColor, XmRString, "#642c2a", 4,
	NULL);
	
}

void activeVolDialog(Widget widget, XtPointer clientData, XEvent *event, Boolean NONE)
{
	Widget volCancel = (Widget)clientData;
	XmProcessTraversal(volCancel, XmTRAVERSE_CURRENT);
}

void volCancelCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	Widget volDialogShell = (Widget)client_data;
	XtDestroyWidget(volDialogShell);  /* close dialog shell */
}

void volDestroyCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	volumeDialogIsOpen = 0;
	
	XtVaSetValues(audioEntry3,
		XmNsensitive, 1,
	NULL);
}

void createVolumeDialog(Widget widget, XtPointer client_data, XtPointer call_data)
{
	if(volumeDialogIsOpen)
	{
		return;
	}
	
	volumeDialogIsOpen = 1;
	
	XtVaSetValues(audioEntry3,
		XmNsensitive, 0,
	NULL);
	
/* initialize a bare dialog shell */
	Widget volDialogShell = XmCreateDialogShell(topLevel, "volDialogShell", NULL, 0);
	XtVaSetValues(volDialogShell,
		XmNtitle, "Local Volume (Not working yet)",
	NULL);
	
	XtAddCallback(volDialogShell, XmNpopdownCallback, volDestroyCallback, (XtPointer)volDialogShell);
	
/* set normal window controls for this dialog */
	int windowDecorations;
	XtVaGetValues(volDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
		windowDecorations &= ~MWM_DECOR_TITLE;
		windowDecorations &= ~MWM_DECOR_BORDER;
		/* windowDecorations &= ~MWM_DECOR_RESIZEH; */
	XtVaSetValues(volDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	
/* create a form to hold the other widgets */
	Widget volDialogForm = XtVaCreateManagedWidget("volDialogForm", xmFormWidgetClass, volDialogShell,
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
	NULL);
	
/* cancels the dialog */
	Widget volCancel = XtVaCreateManagedWidget("volCancel", xmPushButtonWidgetClass, volDialogForm,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 8,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Close", 4,
	NULL);
	
	
	XtAddCallback(volCancel, XmNactivateCallback, volCancelCallback, (XtPointer)volDialogShell);
	
	
	Widget volReset = XtVaCreateManagedWidget("volReset", xmPushButtonWidgetClass, volDialogForm,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, volCancel,
		XmNrightOffset, 5,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 8,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Reset", 4,
	NULL);
	
	Widget volSystem = XtVaCreateManagedWidget("volSystem", xmPushButtonWidgetClass, volDialogForm,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 8,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "System Control Panel...", 4,
	NULL);
	
	Widget volSep0 = XtVaCreateManagedWidget("volSep0", xmSeparatorWidgetClass, volDialogForm,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 1,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 1,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volCancel,
		XmNbottomOffset, 6,
	NULL);

	Widget volScale0 = XtVaCreateManagedWidget("volScale0", xmScaleWidgetClass, volDialogForm,
		XmNorientation, XmVERTICAL,
		XmNminimum, 0,
		XmNmaximum, 100,
		XmNvalue, 100,
		XmNshowValue, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 12,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 16,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volSep0,
		XmNbottomOffset, 10,
		XmNsliderMark, XmETCHED_LINE,
		XmNhighlightThickness, 0,
		XmNscaleWidth, 18,
	NULL);
	XmScaleSetTicks(volScale0, 100, 9, 1, 12, 8, 0);

	/* callback for master scale widget * /
	XtAddCallback(masterScale, XmNvalueChangedCallback, masterScale_callback, NULL);
	*/
	
	Widget colorFrame0 = XtVaCreateManagedWidget("colorFrame0", xmFrameWidgetClass, volDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 13,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, volScale0,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volSep0,
		XmNbottomOffset, 11,
		
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_IN,
	NULL);
	
	createColorBars(colorFrame0, 5);
	
	
	Widget volScale1 = XtVaCreateManagedWidget("volScale1", xmScaleWidgetClass, volDialogForm,
		XmNorientation, XmVERTICAL,
		XmNminimum, 0,
		XmNmaximum, 100,
		XmNvalue, 100,
		XmNshowValue, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 12,
		
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, colorFrame0,
		XmNleftOffset, 220,
		
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volSep0,
		XmNbottomOffset, 10,
		XmNsliderMark, XmETCHED_LINE,
		XmNhighlightThickness, 0,
		XmNscaleWidth, 18,
	NULL);
	XmScaleSetTicks(volScale1, 100, 9, 1, 12, 8, 0);

	/* callback for master scale widget * /
	XtAddCallback(masterScale, XmNvalueChangedCallback, masterScale_callback, NULL);
	*/
	
	Widget colorFrame1 = XtVaCreateManagedWidget("colorFrame1", xmFrameWidgetClass, volDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 13,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, volScale1,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volSep0,
		XmNbottomOffset, 11,
		
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_IN,
	NULL);
	
	createColorBars(colorFrame1, 5);
	
	
	
	Widget volScale2 = XtVaCreateManagedWidget("volScale2", xmScaleWidgetClass, volDialogForm,
		XmNorientation, XmVERTICAL,
		XmNminimum, 0,
		XmNmaximum, 100,
		XmNvalue, 100,
		XmNshowValue, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 12,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, colorFrame1,
		XmNleftOffset, 30,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volSep0,
		XmNbottomOffset, 10,
		XmNsliderMark, XmETCHED_LINE,
		XmNhighlightThickness, 0,
		XmNscaleWidth, 18,
	NULL);
	XmScaleSetTicks(volScale2, 100, 9, 1, 12, 8, 0);

	/* callback for master scale widget * /
	XtAddCallback(masterScale, XmNvalueChangedCallback, masterScale_callback, NULL);
	*/
	
	Widget colorFrame2 = XtVaCreateManagedWidget("colorFrame2", xmFrameWidgetClass, volDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 13,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, volScale2,
		XmNleftOffset, 2,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, volSep0,
		XmNbottomOffset, 11,
		
		
		XmNshadowThickness, 1,
		XmNshadowType, XmSHADOW_IN,
	NULL);
	
	createColorBars(colorFrame2, 5);
	
	
	
	
	Dimension buttonHeight;
	XtVaGetValues(volCancel, XmNheight, &buttonHeight, NULL);
	
	int finalWidth, finalHeight;
	
	finalWidth = 400;
	finalHeight = 198 + 36 + buttonHeight;
	

	

/* get _NET_FRAME_EXTENTS for window border and title bar of the dialog shell */
	int borderTop, borderLeft, borderRight, borderBottom;
	int *extents = getInfoDialogFrameExtents(topLevel);
	if(extents != NULL)
	{
		borderLeft = extents[0];
		borderRight = extents[1];
		borderTop = extents[2];
		borderBottom = extents[3];
	}
	
	
/* figure out where top level window is positioned */
	Dimension topShellW, topShellH, topShellX, topShellY;
	XtVaGetValues(topLevel,
		XmNwidth, &topShellW,
		XmNheight, &topShellH,
		XmNx, &topShellX,
		XmNy, &topShellY,
	NULL);
	
	
	int newX, newY;
	newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
	newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderTop;
	
	
	/* set window geometry and position */
	XtVaSetValues(volDialogShell,
		XmNwidth, finalWidth, 
		XmNheight, finalHeight,
		XmNx, newX,
		XmNy, newY,
	NULL);
	
	
/* give dismiss button default focus */
	XtAddEventHandler(volDialogShell, ExposureMask, False, 
	(XtEventHandler)activeVolDialog, (XtPointer)volCancel);
	
	
}
