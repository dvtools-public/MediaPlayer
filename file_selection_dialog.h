
/* trim off everything before the last directory */
char *trimPathSep(const char *file_path)
{
	const char *last_slash = strrchr(file_path, '/');
	if(last_slash != NULL)
	{
		size_t len = strlen(last_slash);
        
		char *trimmed_path = (char *)malloc(len + 1);
		if(trimmed_path != NULL)
		{
			strncpy(trimmed_path, last_slash + 1, len);
			trimmed_path[len] = '\0';
			return trimmed_path;
		}
	
		else { return NULL; }
	}
	
	else
	{
		char *original_path = (char *)malloc(strlen(file_path) + 1);
		if(original_path != NULL)
		{
			strcpy(original_path, file_path);
			return original_path;
		}
		
		else { return NULL; }
	}
}

void fsCancelCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	selectionDialogIsOpen = 0;
	
	XtVaSetValues(fileOpen,
		XmNsensitive, 1,
	NULL);
	
	Widget fileSelectionDialogShell = (Widget)client_data;
	XtDestroyWidget(fileSelectionDialogShell);  /* close dialog shell */
}

void okCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	Widget fileSelectionBox = (Widget)client_data;
	
	XmString selectedDir;
	XmString selectedFile;
	char *filePath;
	char *fileName;

	XtVaGetValues(fileSelectionBox, XmNdirSpec, &selectedDir, NULL);
	XtVaGetValues(fileSelectionBox, XmNtextString, &selectedFile, NULL);

	if(selectedFile == NULL || XmStringEmpty(selectedFile))
	{
		createQuickErrorDialog("Error", "No media file selected. ");
		return;
	}
	
	if(!XmStringGetLtoR(selectedDir, XmFONTLIST_DEFAULT_TAG, &filePath))
	{
		return;
	}
	
	if(!XmStringGetLtoR(selectedFile, XmFONTLIST_DEFAULT_TAG, &fileName))
	{
		return;
	}

	
	snprintf(fileInputString, sizeof(fileInputString), "%s", filePath);
	
	
	endOfFile = 0;
	isPlaying = 1;
	
	mpv_command_string(mpv, "loadfile \"\""); /* unloads current file if any */
	
	mpv_command_string(mpv, "set pause no");
	
	XtVaSetValues(statusLabel,
		XtVaTypedArg, XmNlabelString, XmRString, "Playing", 4,
	NULL);
	
	XtVaSetValues(togglePlaybackButton,
		XtVaTypedArg, XmNlabelString, XmRString, "Stop Playback", 4,
	NULL);
	
	XtVaSetValues(pauseButton,
		XmNlabelPixmap, pause_pixmap,
	NULL);
	
	
	char tmpBuffer[4096];
	snprintf(tmpBuffer, sizeof(tmpBuffer), "loadfile \"%s\"", fileInputString);
	mpv_command_string(mpv, tmpBuffer);
	
	char *trimmed_path = trimPathSep(fileInputString);
	if (trimmed_path != NULL)
	{
		char titleBuffer[1024];
		snprintf(titleBuffer, sizeof(titleBuffer), "Media Player - %s", trimmed_path),
		
		XtVaSetValues(topLevel,
			XmNtitle, titleBuffer,
		NULL);
		
		free(trimmed_path);
	}
	
	fileIsLoaded = 1;
	wasReset = 1;
	
	
	int widgetSensitivity = 1;
	XtVaSetValues(fileDetails, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(togglePlaybackButton, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(infiniteLoop, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(playbackSpeed, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(seekValue, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(seekBackward, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(seekForward, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(resetReload, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(audioEntry0, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(audioEntry1, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(audioEntry2, XmNsensitive, widgetSensitivity, NULL);
	XtVaSetValues(audioEntry3, XmNsensitive, widgetSensitivity, NULL);
	
	
	
/* clean up */
	XtFree(filePath);
	XtFree(fileName);
	XmStringFree(selectedDir);
	XmStringFree(selectedFile);
	
	
	selectionDialogIsOpen = 0;
	
	XtVaSetValues(fileOpen,
		XmNsensitive, 1,
	NULL);

/* close dialog */
	XtDestroyWidget(fileSelectionDialogShell);
}

void fsDestroyCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
	selectionDialogIsOpen = 0;
	
	XtVaSetValues(fileOpen,
		XmNsensitive, 1,
	NULL);
}

void createTemporaryFilePickerDialog(Widget widget, XtPointer client_data, XtPointer call_data)
{
	if(selectionDialogIsOpen)
	{
		return;
	}
	
	selectionDialogIsOpen = 1;
	
	XtVaSetValues(fileOpen,
		XmNsensitive, 0,
	NULL);
	
	
/* initialize a bare dialog shell */
	fileSelectionDialogShell = XmCreateDialogShell(topLevel, "fileSelectionDialogShell", NULL, 0);
	XtVaSetValues(fileSelectionDialogShell,
		XmNtitle, "Open File",
		
		XmNminWidth, 500,
		XmNminHeight, 400,
	NULL);
	
	XtAddCallback(fileSelectionDialogShell, XmNpopdownCallback, fsDestroyCallback, NULL);
	
	
/* tell vendorshell what kind of window decorations to apply to the dialog */
	int windowDecorations;
	XtVaGetValues(fileSelectionDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
		windowDecorations &= ~MWM_DECOR_TITLE;
		windowDecorations &= ~MWM_DECOR_BORDER;
		windowDecorations &= ~MWM_DECOR_RESIZEH;
	XtVaSetValues(fileSelectionDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	
	
/* create a form to hold the other widgets */
	Widget infoDialogForm = XtVaCreateManagedWidget("infoDialogForm", xmFormWidgetClass, fileSelectionDialogShell,
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
	NULL);
	
/* generic file selection dialog */
	fileSelectionBox = XtVaCreateManagedWidget("fileSelectionBox", xmFileSelectionBoxWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
		
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
		
		XmNfileTypeMask, XmFILE_REGULAR,
		XmNpathMode, XmPATH_MODE_RELATIVE,
		XmNfileFilterStyle, XmFILTER_HIDDEN_FILES,
	NULL);
	
	/* remove useless help button */
	Widget helpButton = XmFileSelectionBoxGetChild(fileSelectionBox, XmDIALOG_HELP_BUTTON);
	XtUnmanageChild(helpButton);
	
	Widget filterButton = XmFileSelectionBoxGetChild(fileSelectionBox, XmDIALOG_APPLY_BUTTON);
	
	XtVaSetValues(filterButton,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
	NULL);
	
	Widget cancelButton = XmFileSelectionBoxGetChild(fileSelectionBox, XmDIALOG_CANCEL_BUTTON);
	XtAddCallback(cancelButton, XmNactivateCallback, fsCancelCallback, (XtPointer)fileSelectionDialogShell);
	
	XtVaSetValues(cancelButton,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
	NULL);
	
	okButton = XmFileSelectionBoxGetChild(fileSelectionBox, XmDIALOG_OK_BUTTON);
	XtAddCallback(okButton, XmNactivateCallback, okCallback, (XtPointer)fileSelectionBox);
	
	XtVaSetValues(okButton,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		XtVaTypedArg, XmNlabelString, XmRString, "Open", 4,
	NULL);
	

/* get _NET_FRAME_EXTENTS for window border and title bar of the top window shell */
	int borderTop, borderLeft, borderRight, borderBottom;
	int *extents = getInfoDialogFrameExtents(topLevel);
	if(extents != NULL)
	{
		borderLeft = extents[0];
		borderRight = extents[1];
		borderTop = extents[2];
		borderBottom = extents[3];
	}
	
	
	
/* figure out where top level window is positioned */
	Dimension topShellW, topShellH, topShellX, topShellY;
	XtVaGetValues(topLevel,
		XmNwidth, &topShellW,
		XmNheight, &topShellH,
		XmNx, &topShellX,
		XmNy, &topShellY,
	NULL);
	
/* fix window dimensions */
	
	int finalWidth, finalHeight;
	
	finalWidth = topShellW / 100 * 70 + 100;
	finalHeight = topShellH / 100 * 60 + 100;
	
	if(finalWidth < 700)
	{
		finalWidth = 700;
	}
	
	if(finalHeight < 500)
	{
		finalHeight = 500;
	}
	
/* figure out where dialog shell window is positioned */
	Dimension infoDialogX, infoDialogY;
	XtVaGetValues(infoDialogForm,
		XmNx, &infoDialogX,
		XmNy, &infoDialogY,
	NULL);
	

	int newX, newY;		
	newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
	newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderTop;
	
	
	
	/* set window geometry and position */
	XtVaSetValues(fileSelectionDialogShell,
		XmNwidth, finalWidth, 
		XmNheight, finalHeight,
		XmNx, newX,
		XmNy, newY,
	NULL);
	
	
	
}



















void createFilePickerDialog(Widget widget, XtPointer client_data, XtPointer call_data)
{
	int frame = 2;
	
	char *winTitle = "Open Media File";
	
/* initialize a bare dialog shell */
	Widget infoDialogShell = XmCreateDialogShell(topLevel, "infoDialogShell", NULL, 0);
	XtVaSetValues(infoDialogShell,
		XmNtitle, winTitle,
	NULL);
	
	
/* tell vendorshell what kind of window decorations to apply to the dialog */
	int windowDecorations;
	
	XtVaGetValues(infoDialogShell, XmNmwmDecorations, &windowDecorations, NULL);
		windowDecorations &= ~MWM_DECOR_MENU;
		windowDecorations &= ~MWM_DECOR_TITLE;
		windowDecorations &= ~MWM_DECOR_BORDER;
		windowDecorations &= ~MWM_DECOR_RESIZEH;
	XtVaSetValues(infoDialogShell, XmNmwmDecorations, windowDecorations, NULL);
	
/* create a form to hold the other widgets */
	Widget infoDialogForm = XtVaCreateManagedWidget("infoDialogForm", xmFormWidgetClass, infoDialogShell,
		XmNshadowType, XmSHADOW_OUT,
		XmNshadowThickness, 1,
	NULL);
	
/* cancels the dialog */
	Widget infoApply = XtVaCreateManagedWidget("infoApply", xmPushButtonWidgetClass, infoDialogForm,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Open File", 4,
	NULL);
	
	Widget infoCancel = XtVaCreateManagedWidget("infoCancel", xmPushButtonWidgetClass, infoDialogForm,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, infoApply,
		XmNrightOffset, 4,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		
		XmNshadowThickness, 2,
		XmNmarginHeight, 4,
		XmNmarginWidth, 7,
		XmNtraversalOn, 1,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Cancel", 4,
	NULL);
	
/* etched in frame around main message */
	Widget infoLabelFrame = XtVaCreateManagedWidget("infoLabelFrame", xmFrameWidgetClass, infoDialogForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 8,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, infoCancel,
		XmNbottomOffset, 6,
		
		XmNshadowType, XmSHADOW_IN,
		XmNshadowThickness, 2,
		
	NULL);
	
	Widget constructionLabel = XtVaCreateManagedWidget("constructionLabel", xmLabelWidgetClass, infoLabelFrame,
		XmNalignment, XmALIGNMENT_CENTER,
		
		XtVaTypedArg, XmNlabelString, XmRString, "This file selection dialog is still under construction.\nMedia Player must be launched with a file path or URL.", 4,
		XtVaTypedArg, XmNbackground, XmRString, "#a2cd5a", 4,
		XtVaTypedArg, XmNforeground, XmRString, "black", 4,
		XmNfontSize, 18,
	NULL);

/* get _NET_FRAME_EXTENTS for window border and title bar of the top window shell */
	int borderTop, borderLeft, borderRight, borderBottom;
	int *extents = getInfoDialogFrameExtents(topLevel);
	if(extents != NULL)
	{
		borderLeft = extents[0];
		borderRight = extents[1];
		borderTop = extents[2];
		borderBottom = extents[3];
	}
	
	
	
/* figure out where top level window is positioned */
	Dimension topShellW, topShellH, topShellX, topShellY;
	XtVaGetValues(topLevel,
		XmNwidth, &topShellW,
		XmNheight, &topShellH,
		XmNx, &topShellX,
		XmNy, &topShellY,
	NULL);
	
/* fix window dimensions */
	Dimension buttonHeight;
	XtVaGetValues(infoCancel, XmNheight, &buttonHeight, NULL);
	
	int finalWidth, finalHeight;
	
	finalWidth = topShellW / 100 * 70 + 100;
	finalHeight = topShellH / 100 * 60 + 100 + buttonHeight;
	
	if(finalWidth < 500)
	{
		finalWidth = 500;
	}
	
	if(finalHeight < 300)
	{
		finalHeight = 300;
	}
	
/* figure out where dialog shell window is positioned */
	Dimension infoDialogX, infoDialogY;
	XtVaGetValues(infoDialogForm,
		XmNx, &infoDialogX,
		XmNy, &infoDialogY,
	NULL);
	

	int newX, newY;		
	newX = topShellX + (topShellW / 2) - (finalWidth / 2) - borderLeft;
	newY = topShellY + (topShellH / 2) - (finalHeight / 2) - borderTop;
	
	
	
	/* set window geometry and position */
	XtVaSetValues(infoDialogShell,
		XmNwidth, finalWidth, 
		XmNheight, finalHeight,
		XmNx, newX,
		XmNy, newY,
	NULL);
	
	
	
/* give dismiss button default focus */
	XtAddEventHandler(infoDialogShell, ExposureMask, False, 
	(XtEventHandler)activeDialog, (XtPointer)infoApply);
}
