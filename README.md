### Media Player

An extremely simple X11/Motif GUI for libmpv 0.32.0+ 

Still a WIP but sort of usable. A compositor such as compton or picom is needed to prevent excessive label flickering. X11 doing things async and waiting for other events to complete is probably what's causing the problem. Swapping out Xt timers for a POSIX thread made no difference.


![screenshot](screenshots/dvmedia_sample0.png)


![screenshot](screenshots/dvmedia_sample1.png)


#### Primary Features

- Play audio and video (tested with mp3, mp4, wav, flac).
- Space or Ctrl + P - Stop or resume playback.
- Set custom playback and seek times.
- Ctrl + R - Pause and reload file.
- Ctrl + L - Loop playback infinitely.
- Ctrl + Z/X - Seek backwards/forwards.
- Ctrl + D - View embedded file data and other details (~30% done).

#### Secondary Features

Right clicking in the video area will toggle play/pause.

- Ctrl + C - Hide lower control area.
- Ctrl + J - Show percentage jump bar.
- Ctrl + S - Keep window at top of stack.
- Ctrl + N - Launch new instance.

#### Coming Soon

- Local volume adjustment.
- Set audio channels (stereo or mono).
- Set audio outputs (left, right, or both).
- Force video aspect ratio.

#### Known Defects

It's not very efficient and may have some small memory leaks. 

Fullscreen video support will not be ready for a long time. It probably won't work outside of EMWM or Dtwm (CDE).

Whole directory handling is not implemented yet. For now, it's only meant to operate on a single media file.


#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXpm
- libXinerama
- libmpv


#### Build Instructions

Compile:

```
make clean && make
```

There is an option to disable the GPU driver and fall back to X11 interfaces with software decoding. Performance will be terrible but it's better than nothing if you're using a virtual machine or headless server.

```
make clean && make CFLAGS+=-D_NO_HARDWARE_ACCEL
```

Run:

```
./dvmedia /path/to/file - name.mp4

   OR

./dvmedia https://www.website.org/url/path/to/fileName.mp4
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```


#### Credits & Licensing

Big thanks to the mpv developers for the amazing media player core.


Only the GUI frontend is distributed under the BSD Zero Clause license.
The libmpv core is GPLv2+ and the client header says ISC.