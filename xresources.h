static String fb_xres[] = 
{
	"dvmedia*infoTextArea*fontStyle: Bold",
	"dvmedia*genLabel0*fontStyle: Bold",
	"dvmedia*Overview*fontStyle: Italic",
	"dvmedia*audioLabel0*fontStyle: Bold",
	"dvmedia*AudioDetails*fontStyle: Italic",
	
	"dvmedia*constructionLabel*fontStyle: Bold",
	
	
	"dvmedia*XmScrollBar*width: 18",
	"dvmedia*XmScrollBar*height: 18",
	"dvmedia*XmScrollBar*sliderMark: XmTHUMB_MARK",
	"dvmedia*XmScrollBar*shadowThickness: 2",
	
	"dvmedia*fullscreenVideoShell*cancelButton*fontSize: 16",
	
/*
	"dvmedia*font: 9x15",
*/
	
	

/* optional color scheme */
/*
	"dvmedia*foreground: cornsilk3",
	"dvmedia*background: gray15",
	"dvmedia*topShadowColor: #939393",
	"dvmedia*bottomShadowColor: #676767",
	"dvmedia*highlightColor: tomato",
	
	"dvmedia*menuBar*XmRowColumn.XmPushButton.topShadowColor: tomato",
	"dvmedia*menuBar*XmRowColumn.XmPushButton.bottomShadowColor: tomato",
	"dvmedia*menuBar*XmRowColumn.seekValue.topShadowColor: tomato",
	"dvmedia*menuBar*XmRowColumn.seekValue.bottomShadowColor: tomato",
	
	"dvmedia*XmPushButton.background: gray25",
	
	"dvmedia*controlForm*troughColor: #723232",
	"dvmedia*controlForm*XmPushButton.foreground: cornsilk3",
	"dvmedia*controlForm*statusLabel1.foreground: cornsilk4",
	"dvmedia*controlForm*statusLabel2.foreground: cornsilk4",
	"dvmedia*controlForm*progressLabel.foreground: #ff7c60",
	"dvmedia*controlForm*statusLabel.foreground: #f19b18",
	
	"dvmedia*masterForm.foreground: gray5",
	
	"dvmedia*sliderJumpButtonContainer*XmPushButton.background: dodgerblue4",
	"dvmedia*sliderJumpButtonContainer*XmPushButton.topShadowColor: dodgerblue3",
	"dvmedia*sliderJumpButtonContainer*XmPushButton.bottomShadowColor: #103559",
*/
	
	NULL,
};



