void speedCallback0()
{
	const char *speed_cmd[] = { "set", "speed", "0.25", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (0.25)", 4,
	NULL);
}

void speedCallback1()
{
	const char *speed_cmd[] = { "set", "speed", "0.5", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (0.5)", 4,
	NULL);
}

void speedCallback2()
{
	const char *speed_cmd[] = { "set", "speed", "0.75", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (0.75)", 4,
	NULL);
}

void speedCallback3()
{
	const char *speed_cmd[] = { "set", "speed", "1.0", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed", 4,
	NULL);
}

void speedCallback4()
{
	const char *speed_cmd[] = { "set", "speed", "1.25", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (1.25)", 4,
	NULL);
}

void speedCallback5()
{
	const char *speed_cmd[] = { "set", "speed", "1.5", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (1.5)", 4,
	NULL);
}

void speedCallback6()
{
	const char *speed_cmd[] = { "set", "speed", "1.75", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (1.75)", 4,
	NULL);
}

void speedCallback7()
{
	const char *speed_cmd[] = { "set", "speed", "2.0", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (2.0)", 4,
	NULL);
}

void speedCallback8()
{
	const char *speed_cmd[] = { "set", "speed", "2.25", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (2.25)", 4,
	NULL);
}

void speedCallback9()
{
	const char *speed_cmd[] = { "set", "speed", "2.5", NULL };
	mpv_command(mpv, speed_cmd);
	
	XtVaSetValues(playbackSpeed,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed (2.5)", 4,
	NULL);
}


void seekTimeCallback0()
{
	seekSeconds = 5;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 5s", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 5s", 4,
	NULL);
	
}

void seekTimeCallback1()
{
	seekSeconds = 10;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 10s", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 10s", 4,
	NULL);
	
}

void seekTimeCallback2()
{
	seekSeconds = 15;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 15s", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 15s", 4,
	NULL);
	
}

void seekTimeCallback3()
{
	seekSeconds = 30;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 30s", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 30s", 4,
	NULL);
	
}

void seekTimeCallback4()
{
	seekSeconds = 60;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 1m", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 1m", 4,
	NULL);
	
}

void seekTimeCallback5()
{
	seekSeconds = 120;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 2m", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 2m", 4,
	NULL);
	
}

void seekTimeCallback6()
{
	seekSeconds = 300;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 5m", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 5m", 4,
	NULL);
	
}

void seekTimeCallback7()
{
	seekSeconds = 600;
	
	XtVaSetValues(seekBackward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 10m", 4,
	NULL);
	
	XtVaSetValues(seekForward,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 10m", 4,
	NULL);
	
}

void installMenus()
{

/* grab background color for later use with widgets */
	Pixel menu_bg, menu_fg, menu_text;
	XtVaGetValues(menuBar, XmNbackground, &menu_bg, NULL);
	XtVaGetValues(mainWin, XmNforeground, &menu_fg, NULL);
	XtVaGetValues(mainWin, XmNbackground, &menu_text, NULL);
	
/* custom arrow pixmap for cascading submenus */
	Pixmap arrow_bitmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), cascade_arrow3_bits, cascade_arrow3_width, cascade_arrow3_height);
	
	
/* pull down form to hold the sub-entries for the File menu */
	fileMenuPane = XmCreatePulldownMenu(menuBar, "fileMenuPane", NULL, 0);
	XtVaSetValues(fileMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

/* XmCascadeButton for the visible File entry on the menuBar
   It posts the drop down fileMenuPane when activated  */
	Widget fileMenu = XtVaCreateManagedWidget("File", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, fileMenuPane,
		XmNmnemonic, 'F',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
		XmNtopOffset, 20,
	NULL);
	
	
	fileOpen = XtVaCreateManagedWidget("fileOpen", xmPushButtonWidgetClass, fileMenuPane,
		XmNmnemonic, 'O',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>O", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + O ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Open Media File...", 4,
	NULL);
	/*
	XtAddCallback(fileOpen, XmNactivateCallback, createFilePickerDialog, NULL);
	*/
	
	fileDetails = XtVaCreateManagedWidget("fileDetails", xmPushButtonWidgetClass, fileMenuPane,
		XmNmnemonic, 'D',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>D", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + D ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Show File Details...", 4,
	NULL);
	
	Widget fileSep = XtVaCreateManagedWidget("fileSep", xmSeparatorWidgetClass, fileMenuPane, NULL);
	
	Widget fileQuit = XtVaCreateManagedWidget("fileQuit", xmPushButtonWidgetClass, fileMenuPane,
		XmNmnemonic, 'Q',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>Q", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + Q ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Quit", 4,
	NULL);
	XtAddCallback(fileQuit, XmNactivateCallback, instantQuit, NULL);
	
/* beginning of all Playback related menus */
	pbMenuPane = XmCreatePulldownMenu(menuBar, "optMenuPane", NULL, 0);
	XtVaSetValues(pbMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	Widget pbMenu = XtVaCreateManagedWidget("Playback", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, pbMenuPane,
		XmNmnemonic, 'P',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	togglePlaybackButton = XtVaCreateManagedWidget("togglePlaybackButton", xmPushButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'P',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>P", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + P ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Resume Playback", 4,
	NULL);
	
	infiniteLoop = XtVaCreateManagedWidget("infiniteLoop", xmPushButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'L',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>L", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + L ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Loop Infinitely", 4,
	NULL);
	
	
	playbackSpeed = XtVaCreateManagedWidget("playbackSpeed", xmCascadeButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'S',
		XmNmarginWidth, 6,
		XmNmarginHeight, 6,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Playback Speed", 4,
		XmNcascadePixmap, arrow_bitmap,
	NULL);
	
	Widget pbSubMenu0 = XmCreatePulldownMenu(pbMenuPane, "pbSubMenu0", NULL, 0);
	XtVaSetValues(pbSubMenu0,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
		
		XmNradioBehavior, 1,
		XmNradioAlwaysOne, 1,
	NULL);
	/* attach submenu to menu */
	XtVaSetValues(playbackSpeed, XmNsubMenuId, pbSubMenu0, NULL);
	
	
	
	speedValue0 = XtVaCreateManagedWidget("speedValue0", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "0.25", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue1 = XtVaCreateManagedWidget("speedValue1", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "0.5", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue2 = XtVaCreateManagedWidget("speedValue2", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "0.75", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue3 = XtVaCreateManagedWidget("speedValue3", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "1.0", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 1,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue4 = XtVaCreateManagedWidget("speedValue4", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "1.25", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue5 = XtVaCreateManagedWidget("speedValue5", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "1.5", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue6 = XtVaCreateManagedWidget("speedValue6", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "1.75", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue7 = XtVaCreateManagedWidget("speedValue7", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "2.0", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue8 = XtVaCreateManagedWidget("speedValue8", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "2.25", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	speedValue9 = XtVaCreateManagedWidget("speedValue9", xmToggleButtonWidgetClass, pbSubMenu0,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "2.5", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	
	/*
	speedValue0 = XtVaCreateManagedWidget("speedValue0", xmPushButtonWidgetClass, pbSubMenu0,
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "speedValue0", 4,
	NULL);
	*/
	
	
	Widget pbSep0 = XtVaCreateManagedWidget("pbSep0", xmSeparatorWidgetClass, pbMenuPane, NULL);
	
	seekBackward = XtVaCreateManagedWidget("seekBackward", xmPushButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'B',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>Z", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + Z ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Backward 10s", 4,
	NULL);
	
	seekForward = XtVaCreateManagedWidget("seekForward", xmPushButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'F',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>X", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + X ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Forward 10s", 4,
	NULL);
	
	
/* add drop down menu */
	seekValue = XtVaCreateManagedWidget("seekValue", xmCascadeButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'T',
		XmNmarginWidth, 6,
		XmNmarginHeight, 6,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Seek Time", 4,
		XmNcascadePixmap, arrow_bitmap,
	NULL);
	
	Widget pbSubMenu1 = XmCreatePulldownMenu(pbMenuPane, "pbSubMenu1", NULL, 0);
	XtVaSetValues(pbSubMenu1,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
		
		XmNradioBehavior, 1,
		XmNradioAlwaysOne, 1,
	NULL);
	/* attach submenu to menu */
	XtVaSetValues(seekValue, XmNsubMenuId, pbSubMenu1, NULL);
	
	
	
	seekValue0 = XtVaCreateManagedWidget("seekValue0", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "5 seconds", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue1 = XtVaCreateManagedWidget("seekValue1", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "10 seconds", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 1,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue2 = XtVaCreateManagedWidget("seekValue2", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "15 seconds", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue3 = XtVaCreateManagedWidget("seekValue3", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "30 seconds", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue4 = XtVaCreateManagedWidget("seekValue4", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "1 minute", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue5 = XtVaCreateManagedWidget("seekValue5", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "2 minutes", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue6 = XtVaCreateManagedWidget("seekValue6", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "5 minutes", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	seekValue7 = XtVaCreateManagedWidget("seekValue7", xmToggleButtonWidgetClass, pbSubMenu1,
		XmNmarginTop, 5,
		XmNmarginLeft, 8,
		XmNmarginRight, 6,
		XmNmarginBottom, 5,
		XmNmarginWidth, 8,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "10 minutes", 4,
		XmNindicatorSize, 16,
		XmNspacing, 8,
		XmNset, 0,
		XmNindicatorType, XmONE_OF_MANY_ROUND,
	NULL);
	
	
	
	
	
	Widget pbSep1 = XtVaCreateManagedWidget("pbSep1", xmSeparatorWidgetClass, pbMenuPane, NULL);
	
	resetReload = XtVaCreateManagedWidget("resetReload", xmPushButtonWidgetClass, pbMenuPane,
		XmNmnemonic, 'R',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>R", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + R ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Reset & Reload File", 4,
	NULL);
	
	
	
/* beginning of all Options related menus * /
	optMenuPane = XmCreatePulldownMenu(menuBar, "optMenuPane", NULL, 0);
	XtVaSetValues(optMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	optEntry0 = XtVaCreateManagedWidget("winEntry0", xmPushButtonWidgetClass, optMenuPane,
		XmNmnemonic, 'A',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>A", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + A", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Stretch Aspect Ratio", 4,
	NULL);
	
	Widget optMenu = XtVaCreateManagedWidget("Options", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, optMenuPane,
		XmNmnemonic, 'O',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
*/



/* beginning of all Audio related menus */
	audioMenuPane = XmCreatePulldownMenu(menuBar, "audioMenuPane", NULL, 0);
	XtVaSetValues(audioMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	Widget audioMenu = XtVaCreateManagedWidget("Audio", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, audioMenuPane,
		XmNmnemonic, 'A',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	audioEntry0 = XtVaCreateManagedWidget("audioEntry0", xmPushButtonWidgetClass, audioMenuPane,
		XmNmnemonic, 'M',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>M", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + M ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Mute", 4,
	NULL);
	
	audioEntry3 = XtVaCreateManagedWidget("audioEntry3", xmPushButtonWidgetClass, audioMenuPane,
		XmNmnemonic, 'V',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>V", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + V ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Local Volume...", 4,
	NULL);
	
	Widget audioSep1 = XtVaCreateManagedWidget("audioSep1", xmSeparatorWidgetClass, audioMenuPane, NULL);
	
	audioEntry1 = XtVaCreateManagedWidget("audioEntry1", xmCascadeButtonWidgetClass, audioMenuPane,
		XmNmnemonic, 'O',
		XmNmarginWidth, 6,
		XmNmarginHeight, 6,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Outputs", 4,
		XmNcascadePixmap, arrow_bitmap,
	NULL);
	
	Widget audioSubMenu0 = XmCreatePulldownMenu(audioMenuPane, "audioSubMenu0", NULL, 0);
	XtVaSetValues(audioSubMenu0,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
	NULL);
	/* attach submenu to menu */
	XtVaSetValues(audioEntry1, XmNsubMenuId, audioSubMenu0, NULL);

	Widget outputAll = XtVaCreateManagedWidget("outputAll", xmPushButtonWidgetClass, audioSubMenu0,
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Enable All", 4,
	NULL);
	
	Widget outputLeft = XtVaCreateManagedWidget("outputLeft", xmPushButtonWidgetClass, audioSubMenu0,
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Left Only", 4,
	NULL);
	
	Widget outputRight = XtVaCreateManagedWidget("outputRight", xmPushButtonWidgetClass, audioSubMenu0,
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Right Only", 4,
	NULL);
	
	audioEntry2 = XtVaCreateManagedWidget("audioEntry2", xmCascadeButtonWidgetClass, audioMenuPane,
		XmNmnemonic, 'C',
		XmNmarginWidth, 6,
		XmNmarginHeight, 6,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Channels", 4,
		XmNcascadePixmap, arrow_bitmap,
	NULL);
	
	Widget audioSubMenu1 = XmCreatePulldownMenu(audioMenuPane, "audioSubMenu1", NULL, 0);
	XtVaSetValues(audioSubMenu1,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
	NULL);
	/* attach submenu to menu */
	XtVaSetValues(audioEntry2, XmNsubMenuId, audioSubMenu1, NULL);
	
	Widget outputStereo = XtVaCreateManagedWidget("outputStereo", xmPushButtonWidgetClass, audioSubMenu1,
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Stereo", 4,
	NULL);
	
	Widget outputMono = XtVaCreateManagedWidget("outputMono", xmPushButtonWidgetClass, audioSubMenu1,
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNlabelString, XmRString, "Mono", 4,
	NULL);
	
	
/* beginning of all Video related menus */
	videoMenuPane = XmCreatePulldownMenu(menuBar, "videoMenuPane", NULL, 0);
	XtVaSetValues(videoMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	Widget videoMenu = XtVaCreateManagedWidget("Video", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, videoMenuPane,
		XmNmnemonic, 'V',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	videoEntry0 = XtVaCreateManagedWidget("videoEntry0", xmPushButtonWidgetClass, videoMenuPane,
		XmNmnemonic, 'F',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>F", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + F ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Fullscreen", 4,
	NULL);
	XtAddCallback(videoEntry0, XmNactivateCallback, toggleFullscreen, NULL);




/* beginning of all Window related menus */
	winMenuPane = XmCreatePulldownMenu(menuBar, "winMenuPane", NULL, 0);
	XtVaSetValues(winMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget winMenu = XtVaCreateManagedWidget("Window", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, winMenuPane,
		XmNmnemonic, 'W',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	winEntry2 = XtVaCreateManagedWidget("winEntry2", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'C',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>C", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + C ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Hide Lower Control Area", 4,
	NULL);
	XtAddCallback(winEntry2, XmNactivateCallback, toggleLowerControlArea, NULL);
	
	winEntry3 = XtVaCreateManagedWidget("winEntry3", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'J',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>J", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + J ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Show Jump Bar", 4,
	NULL);
	XtAddCallback(winEntry3, XmNactivateCallback, togglePercBar, NULL);
	
	winEntry0 = XtVaCreateManagedWidget("winEntry0", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'S',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>S", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + S ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Keep Top of Stack", 4,
	NULL);
	XtAddCallback(winEntry0, XmNactivateCallback, toggleAlwaysOnTop, NULL);
	
	Widget winSep = XtVaCreateManagedWidget("winSep", xmSeparatorWidgetClass, winMenuPane, NULL);
	
	winEntry1 = XtVaCreateManagedWidget("winEntry1", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'N',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>N", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + N ", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Launch New Instance...", 4,
	NULL);
	XtAddCallback(winEntry1, XmNactivateCallback, newInstance, NULL);
	

/* beginning of all Help related menus */
	Widget helpMenuPane = XmCreatePulldownMenu(menuBar, "helpMenuPane", NULL, 0);
	XtVaSetValues(helpMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget helpMenu = XtVaCreateManagedWidget("Help", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, helpMenuPane,
		XmNmnemonic, 'H',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget helpEntry0 = XtVaCreateManagedWidget("helpEntry0", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'A',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		
		XtVaTypedArg, XmNlabelString, XmRString, "About", 4,
	NULL);
	XtAddCallback(helpEntry0, XmNactivateCallback, aboutButtonCallback, (XtPointer)topLevel);
	
	Widget helpEntry1 = XtVaCreateManagedWidget("helpEntry1", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'L',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		
		XtVaTypedArg, XmNlabelString, XmRString, "License", 4,
	NULL);
	XtAddCallback(helpEntry1, XmNactivateCallback, licenseButtonCallback, (XtPointer)topLevel);
	
	Widget helpEntry2 = XtVaCreateManagedWidget("helpEntry2", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'U',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		
		XtVaTypedArg, XmNlabelString, XmRString, "Usage", 4,
	NULL);
	XtAddCallback(helpEntry2, XmNactivateCallback, usageButtonCallback, (XtPointer)topLevel);

/* let the menu bar know we want help on the right */
	XtVaSetValues(menuBar,
		XmNmenuHelpWidget, helpMenu,
	NULL);
}
